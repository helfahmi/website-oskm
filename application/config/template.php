<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'default';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['default']['template'] = 'template';
$template['default']['regions'] = array('main_nav','background','content','footer');

$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;




$template['admin']['template'] = 'template_admin';
$template['admin']['regions'] = array('main_nav','home_script','side_nav','background','content','footer');

$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = FALSE;


$template['gallery']['template'] = 'template_albums';
$template['gallery']['regions'] = array('main_nav','gallery_content','content','footer');

$template['gallery']['parser'] = 'parser';
$template['gallery']['parser_method'] = 'parse';
$template['gallery']['parse_template'] = FALSE;


$template['home']['template'] = 'template_home';
$template['home']['regions'] = array('main_nav','side_nav','background','content','footer');

$template['home']['parser'] = 'parser';
$template['home']['parser_method'] = 'parse';
$template['home']['parse_template'] = FALSE;



$template['reward']['template'] = 'template_rewar';
$template['reward']['regions'] = array('main_nav','side_nav','background','content','footer');

$template['reward']['parser'] = 'parser';
$template['reward']['parser_method'] = 'parse';
$template['reward']['parse_template'] = FALSE;



$template['single']['template'] = 'coba_template';
$template['single']['regions'] = array('primary_nav','news','common_info','blogs','gallery','footer');

$template['single']['parser'] = 'parser';
$template['single']['parser_method'] = 'parse';
$template['single']['parse_template'] = FALSE;



// $template['default']['regions']['main_nav'] = array('content' => array('this is default navbar when not set'), 'wrapper' => '<div>' , 'attributes' => array( 'class' => 'navbar navbar-inverse navbar-fixed-top'));
// $template['default']['regions']['background'] = array('content' => array('<h1>this is bacground default!</h1>'), 'wrapper' => '<div>' , 'attributes' => array('class' => 'carousel container slide'));
// $template['default']['regions']['content'] = array('content' => array('<h1>This is when content is empty</h1>'), 'wrapper' => '<div>' , 'attributes' => array('class' => 'container '));
// $template['default']['regions']['footer'] = array('content' => array('<h1>Footer kosong dkaya begini nih </h1>'));


// $template['default']['regions']['main_nav']=array('content'=>'this is default content of main_nav when not set');
// $template['default']['regions']['second_nav']=array('content'=>'this is default content of second_nav when not set');
// $template['default']['regions']['background']=array('content'=>'this is default content of background when not set');
// $template['default']['regions']['content']=array('content'=>'this is default content of content when not set');
// $template['default']['regions']['footer']=array('content'=>'this is default content of footer when not set');


// BACKUP
// $template['default']['template'] = 'template';
// $template['default']['regions'] = array(
//    'header',
//    'content',
//    'footer',
// );
// $template['default']['parser'] = 'parser';
// $template['default']['parser_method'] = 'parse';
// $template['default']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */