<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI

class Admin extends CI_Controller {

 function __construct()
 {
    parent::__construct();
    /* Standard Libraries of codeigniter are required */
    $this->load->database();
    $this->load->helper('url');
    /* ------------------ */ 

    $this->load->library('grocery_CRUD');
 }
 
 function valid(){
    return $this->session->userdata('logged_in');
 }

 function index(){
   if($this->valid()){
     $session_data = $this->session->userdata('logged_in');

     $data['message'] = $this->session->userdata('message');
     $this->session->set_userdata('message', '');
     $data['username'] = $session_data['username'];
     $data['curdir'] = "index";
     

     // MUST BE MODIFIED BY FAWWAZ
     $this->template->set_template('admin');
     $this->template->add_css('/assets/css/admin_sidenav.css');
     $this->template->write_view('main_nav','component/navbar',null,true);
     $this->template->write_view('background','component/background',null,true);
     $this->template->write_view('side_nav','admin/sidebar',$data,true);
     $this->template->write_view('content','admin/welcome',null,true);
     $this->template->render();

     // $this->load->view('admin_view_sidebar', $data);
     // $this->load->view('admin_view_index', $data);
   } else {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function removeRaquo($divisi){
  if(substr($divisi, 0, 7) == "&raquo;")
	$divisi = substr($divisi, 8);
  return $divisi;
 }
 
 function export($type){
  if($this->valid()){

    if($type == 'univ'){
      //Tabel universal

      //TAB DELIMITED EXPORT
      //create query to select as data from your table\
      //>> Nim, Nama,  Kelompok, Fakultas, Pilihan divisi 1-6
      $select = "SELECT myalias2.nim, myalias2.nama, kelompok.no_klp, myalias2.fakultas, myalias2.Pilihan1, myalias2.Pilihan2, myalias2.Pilihan3 FROM kelompok RIGHT JOIN (SELECT * FROM mahasiswa NATURAL JOIN (SELECT a.nim, b.nama AS 'Pilihan1', c.nama AS 'Pilihan2', d.nama AS 'Pilihan3' FROM memilih a JOIN divisi b on a.pil1 = b.id JOIN divisi c on a.pil2 = c.id JOIN divisi d on a.pil3 = d.id) AS myalias) AS myalias2 ON kelompok.nim = myalias2.nim;";

      //run mysql query and then count number of fields
      $query = $this->db->query($select);
      $result = $query->result_array();
      
      $fields = array("NIM","Nama","No. Kelompok","Fakultas","Pilihan 1","Pilihan 2","Pilihan 3");

      //create csv header row, to contain table headers
      //with database field names
      $header = "";
      foreach($fields as $field){
        $header .= $field."\t";
      }
      //echo $header;

      //this is where most of the work is done. 
      //Loop through the query results, and create 
      //a row for each
      $data = '';
      foreach($result as $row){
        $line = '';
        foreach( $row as $value ) {
          //if null, create blank field
          if ( ( !isset( $value ) ) || ( $value == "" ) ){
            $value = "\t";
          }
          //else, assign field value to our data
          else {
            //$value = str_replace( '"' , '""' , $value );
            $value = $this->removeRaquo($value)."\t";
          }
          //add this field value to our row
          $line .= $value;
        }
        //trim whitespace from each row
        $data .= trim( $line ) . "\n";
      }
      //remove all carriage returns from the data
      $data = str_replace( "\r" , "" , $data );


      //create a file and send to browser for user to download
      header("Content-type: application/vnd.ms-excel; charset=iso-8859-1");
      header("Content-disposition: xls" . date("Y-m-d") . ".xls");
      header( "Content-disposition: filename=exportOSKM2013-".date("dmY")."universal.xls");
      print "$header\n$data";
    } else if($type == 'int'){
      //Tabel integrasi kelompok
      
      //TAB DELIMITED EXPORT
      //create query to select as data from your table
      //>> kolom-kolom dari tabel mahasiswa + nomor kelompok
      $select = "SELECT mahasiswa.nim, mahasiswa.nama, mahasiswa.fakultas, mahasiswa.asalsma, mahasiswa.hp, mahasiswa.alamat, mahasiswa.penyakit, mahasiswa.email, mahasiswa.jenis_kelamin, mahasiswa.agama, mahasiswa.jurusan, kelompok.no_klp AS 'Nomor Kelompok' FROM mahasiswa LEFT JOIN kelompok ON mahasiswa.nim = kelompok.nim ORDER BY kelompok.no_klp ASC;";

      //run mysql query and then count number of fields
      $query = $this->db->query($select);
      $result = $query->result_array();
      
      //nama fields
      $fields = array("NIM","Nama","Fakultas","Asal SMA","HP","Alamat","Penyakit","E-mail","Jenis Kelamin","Agama","Jurusan","Nomor Kelompok");

      //create csv header row, to contain table headers
      //with database field names
      $header = "";
      foreach($fields as $field){
        $header .= $field."\t";
      }
      //echo $header;

      //this is where most of the work is done. 
      //Loop through the query results, and create 
      //a row for each
      $data = '';
      foreach($result as $row){
        $line = '';
        foreach( $row as $value ) {
          //if null, create blank field
          if ( ( !isset( $value ) ) || ( $value == "" ) ){
            $value = "\t";
          }
          //else, assign field value to our data
          else {
            //$value = str_replace( '"' , '""' , $value );
            $value = $this->removeRaquo($value)."\t";
          }
          //add this field value to our row
          $line .= $value;
        }
        //trim whitespace from each row
        $data .= trim( $line ) . "\n";
      }
      //remove all carriage returns from the data
      $data = str_replace( "\r" , "" , $data );


      //create a file and send to browser for user to download
      header("Content-type: application/vnd.ms-excel; charset=iso-8859-1");
      header("Content-disposition: xls" . date("Y-m-d") . ".xls");
      header( "Content-disposition: filename=exportOSKM2013-".date("dmY")."kelompok.xls");
      print "$header\n$data";
    }
  } else {
     //If no session, redirect to login page
     redirect('site', 'refresh');
  }
 }
 
 function create(){
  if($this->valid()){
    $session_data = $this->session->userdata('logged_in');
    
    $data['message'] = $this->session->userdata('message');
    $this->session->set_userdata('message', '');
    $data['username'] = $session_data['username'];
    $data['curdir'] = "create";
    $data['action'] = "/admin/post";
    $data['title'] = "";
    $data['content'] = "";

    // $this->load->view('admin_view_sidebar', $data);
    //ADDED by FAWWAZ
    $this->template->set_template('admin');
    $this->template->add_css('/assets/css/admin_sidenav.css');
    $this->template->write_view('main_nav','component/navbar',null,true);
    $this->template->write_view('background','component/background',null,true);
    $this->template->write_view('side_nav','admin/sidebar',$data,true);
    $this->template->write_view('content','admin/admin_view_create',$data,true);
    $this->template->render();
    // $this->load->view('admin_view_create', $data);


  } else {
     //If no session, redirect to login page
     redirect('login', 'refresh');
  }
 }

 function post(){
  if($this->valid()){
    $session_data = $this->session->userdata('logged_in');

    $this->form_validation->set_message('check_empty', 'Judul dan konten tidak boleh keduanya kosong.');

    $this->form_validation->set_rules('title', 'Judul', 'trim|required|xss_clean');
    $this->form_validation->set_rules('content', 'Konten', 'trim|required|xss_clean|callback_check_empty[title]');

    date_default_timezone_set('Asia/Jakarta');
    $date = date("Y-m-d");
    $title = $this->input->post('title');
    $content = $this->input->post('content');
    $username = $session_data['username'];

    if($this->form_validation->run() == FALSE){ 
       //Field validation failed. 
      //ADDED by FAWWAZ
      $this->template->set_template('admin');
      $this->template->add_css('/assets/css/admin_sidenav.css');
      $this->template->write_view('main_nav','component/navbar',null,true);
      $this->template->write_view('background','component/background',null,true);
      $this->template->write_view('side_nav','admin/sidebar',true);
      $this->template->write_view('content','admin/admin_view_create',true);
      $this->template->render();
       // $this->load->view('admin_view_sidebar', $data);
       // $this->load->view('admin_view_create', $data);
    } else {
       //Field validation success. Insert data.
       $query = "INSERT INTO posts(author, date, title, content) VALUES (?, ?, ?, ?)";  
       //automatically escaped by CI
       $this->db->query($query, array($username, $date, $title, $content));

       redirect('admin/index', 'refresh');
    }    
  } else {
    //tidak terotentikasi
    redirect('login', 'refresh');
  }
 }

 function check_empty($content, $title){
  if($content == NULL && $title == NULL)
    return false;
  return true;
 }

 function viewall(){
  $query = $this->db->get('posts');
  $data['message'] = $this->session->userdata('message');
  $this->session->set_userdata('message', '');
  $data['posts'] = $query->result_array();
  //ADDED by FAWWAZ
  $this->template->set_template('admin');
  $this->template->add_css('/assets/css/admin_sidenav.css');
  $this->template->write_view('main_nav','component/navbar',null,true);
  $this->template->write_view('background','component/background',null,true);
  $this->template->write_view('side_nav','admin/sidebar',null,true);
  $this->template->write_view('content','admin/admin_view_posts',$data,true);
  $this->template->render();
 }

 function delete($post_id){
  $result = $this->db->query("DELETE FROM posts WHERE id = $post_id");
  if($result)
    $this->session->set_userdata('message', 'Tulisan berhasil dihapus');
  else
    $this->session->set_userdata('message', 'Tulisan gagal dihapus');
  redirect('admin/viewall', 'refresh');
 }

 function edit($post_id){
  if($this->valid()){
    $session_data = $this->session->userdata('logged_in');
    $query = $this->db->query("SELECT title, content FROM posts WHERE id = $post_id");
    $result = $query->row();
    $data['message'] = $this->session->userdata('message');
    $this->session->set_userdata('message', '');
    $data['username'] = $session_data['username'];
    $data['curdir'] = "create";
    $data['action'] = "/admin/editpost/$post_id";
    $data['title'] = $result->title;
    $data['content'] = $result->content;

    $this->template->set_template('admin');
    $this->template->add_css('/assets/css/admin_sidenav.css');
    $this->template->write_view('main_nav','component/navbar',null,true);
    $this->template->write_view('background','component/background',null,true);
    $this->template->write_view('side_nav','admin/sidebar',null,true);
    $this->template->write_view('content','admin/admin_view_create',$data,true);
    $this->template->render();
     
   // $this->load->view('admin_view_sidebar', $data);
    // $this->load->view('admin_view_create', $data);
  } else {
     //If no session, redirect to login page
     redirect('login', 'refresh');
  }
 }

 function editpost($post_id){
  if($this->valid()){
    $session_data = $this->session->userdata('logged_in');

    $title = $this->input->post('title');
    $content = $this->input->post('content');
    $date = date("Y-m-d");
    $username = $session_data['username'];

    if($title != NULL && $content != NULL){
      date_default_timezone_set('Asia/Jakarta');
      $query = "UPDATE posts SET author = '$username', title = '$title', content = '$content', date = '$date' WHERE id = $post_id"; 
      $this->session->set_userdata('message', 'Tulisan Anda berhasil diedit');
      $this->db->query($query);
      redirect('admin/viewall', 'refresh');
    } else {
      $this->session->set_userdata('message', 'Post Anda tidak valid');
      redirect('admin/create', 'refresh');
    }
  } else {
     //If no session, redirect to login page
     redirect('login', 'refresh');
  }
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   $this->session->unset_userdata('message');
   session_destroy();
   redirect('login', 'refresh');
 }

 public function managemahasiswa(){
 if($this->valid()){
    $this->grocery_crud->set_subject('Mahasiswa');
    $this->grocery_crud->set_table('mahasiswa');
    //$this->grocery_crud->columns('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    //$this->grocery_crud->fields('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    
     $this->grocery_crud->set_rules('nama', 'Nama lengkap', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('nim', 'NIM', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('fakultas', 'Fakultas', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('asalsma', 'Asal SMA', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('hp', 'No. HP', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('penyakit', 'Penyakit', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
     $this->grocery_crud->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|xss_clean');
     $this->grocery_crud->set_rules('agama', 'Agama', 'trim|required|xss_clean');

    //$this->grocery_crud->set_primary_key('nim');
    $this->grocery_crud->change_field_type('jenis_kelamin', 'enum', array('Pria' => 'Pria', 'Wanita' => 'Wanita'));
    $this->grocery_crud->change_field_type('agama', 'enum', array('' => '', 'Islam', 'Kristen Katolik', 'Kristen Protestan', 'Buddha', 'Hindu', 'Konghucu'));
    $output = $this->grocery_crud->render();

    $this->_example_output($output); 
    } else {
    	redirect('login', 'refresh');
    }
 }

 function unik($nim){
    $query = $this->db->query("SELECT nim FROM mahasiswa WHERE nim = $nim");
    //echo "FUNGSI UNIK KENA CALLBACK KYA, dengan nim = $nim, dengan nim ini di database udah ada ". $query->num_rows() . " orang<br/>";
    if($query->num_rows() == 0)
      return TRUE; //belum ada, valid
    else {
      $this->form_validation->set_message('unik', 'NIM yang Anda masukkan sudah terdaftar.');
      return FALSE;
    }
  }

 public function managedivisi(){
 if($this->valid()){
    $this->grocery_crud->set_subject('Divisi');
    $this->grocery_crud->set_table('divisi');
    //$this->grocery_crud->columns('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    //$this->grocery_crud->fields('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    
    //$this->grocery_crud->set_primary_key('nim');
    //$this->grocery_crud->change_field_type('jenis_kelamin', 'enum', array('Pria' => 'Pria', 'Wanita' => 'Wanita'));
    
    $output = $this->grocery_crud->render();

    $this->_example_output($output); 
    } else {
    	redirect('login','refresh');
    }
 }

 public function managememilih(){
 if($this->valid()){
    $this->grocery_crud->set_subject('Memilih');
    $this->grocery_crud->set_table('memilih');
    //$this->grocery_crud->columns('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    //$this->grocery_crud->fields('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    
    //$this->grocery_crud->set_primary_key('nim');
    //$this->grocery_crud->change_field_type('jenis_kelamin', 'enum', array('Pria' => 'Pria', 'Wanita' => 'Wanita'));
    
    $output = $this->grocery_crud->render();

    $this->_example_output($output); 
    } else {
    	redirect('login', 'refresh');
    }
 }

 public function managekelompok(){
 if($this->valid()){
    $this->grocery_crud->set_subject('Kelompok');
    $this->grocery_crud->set_table('kelompok');
    //$this->grocery_crud->columns('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    //$this->grocery_crud->fields('nama', 'nim', 'fakultas', 'hp', 'alamat', 'penyakit', 'email', 'jenis_kelamin','agama','jurusan');
    
    //$this->grocery_crud->set_primary_key('nim');
    //$this->grocery_crud->change_field_type('jenis_kelamin', 'enum', array('Pria' => 'Pria', 'Wanita' => 'Wanita'));
    
    $output = $this->grocery_crud->render();

    $this->_example_output($output); 
    } else {
    redirect('login', 'refresh');
    }
 }

 function _example_output($output = null){
    $this->template->set_template('admin');
    $this->template->add_css('/assets/css/admin_sidenav.css');
    $this->template->write_view('main_nav','component/navbar',null,true);
    $this->template->write_view('background','component/background',null,true);
    $this->template->write_view('side_nav','admin/sidebar',null,true);
    $this->template->write_view('content','templates/grocery_template.php',$output,true);
    $this->template->render();
 }

 function reward($opt = null){
 if($this->valid()){
    if($opt == null){
      
      $query = $this->db->query("SELECT * FROM reward ORDER BY id DESC LIMIT 1"); //ambil yg paling baru
      $results = $query->row_array();
      $data['date'] = $results['date_added'];




      $i = 0;
      foreach($results as $col){
        $data['award'][$i] = $col;
        if($i >= 2){ //kolom awardX -- nim 
          $query2 = $this->db->query("SELECT * FROM mahasiswa WHERE NIM = ". $data['award'][$i]);
          $data['info'][$i] = $query2->row_array();
        }
        $i++;
      }
      //ADDED By FAWWAZ
      $this->template->set_template('admin');
      $this->template->add_css('/assets/css/admin_sidenav.css');
      $this->template->write_view('main_nav','component/navbar',null,true);
      $this->template->write_view('background','component/background',null,true);
      $this->template->write_view('side_nav','admin/reward_view', $data,true);
      $this->template->write_view('content','templates/grocery_template.php',$output,true);
      $this->template->render();

    } else if($opt == 'edit'){
      $this->template->set_template('admin');
      $this->template->add_css('/assets/css/admin_sidenav.css');
      $this->template->write_view('main_nav','component/navbar',null,true);
      $this->template->write_view('background','component/background',null,true);
      $this->template->write_view('side_nav','admin/reward_view', $data,true);
      // $this->template->write_view('content','templates/grocery_template',$output,true);
      $this->template->write_view('content','Cek file /template/grocer_template belum ada isi apa apa');
      $this->template->render();      
      $this->load->view('admin/reward_view_edit');
    
    } else if($opt == 'add'){
      $this->template->set_template('admin');
      $this->template->add_css('/assets/css/admin_sidenav.css');
      $this->template->write_view('main_nav','component/navbar',null,true);
      $this->template->write_view('background','component/background',null,true);
      $this->template->write_view('side_nav','admin/sidebar',true);
      $this->template->write_view('content','admin/reward_view_add',true);
      $this->template->render();
    
    } else if($opt == 'verify'){

       $this->form_validation->set_message('cek_terdaftar', 'Salah satu NIM yang Anda masukkan belum terdaftar');
       
       for($i=1;$i<=10;$i++)
         $this->form_validation->set_rules('award'.$i, 'Award ke-'.$i, 'trim|required|xss_clean|is_natural|exact_length[8]|callback_cek_terdaftar');
      
       if($this->form_validation->run() == false){
          //gagal
          $this->template->set_template('admin');
          $this->template->add_css('/assets/css/admin_sidenav.css');
          $this->template->write_view('main_nav','component/navbar',null,true);
          $this->template->write_view('background','component/background',null,true);
          $this->template->write_view('side_nav','admin/sidebar',true);
          $this->template->write_view('content','admin/reward_view_add',true);
          $this->template->render();
       } else {
          //berhasil
          date_default_timezone_set('Asia/Jakarta');
          $date = date("Y-m-d");
          $arr = array($date);
          for($i=1;$i<=10;$i++)
            array_push($arr, $this->input->post('award'.$i));
          $query = "INSERT INTO reward VALUES (null,?,?,?,?,?,?,?,?,?,?,?)";
          $this->db->query($query, $arr);
          $this->template->set_template('admin');
          $this->template->add_css('/assets/css/admin_sidenav.css');
          $this->template->write_view('main_nav','component/navbar',null,true);
          $this->template->write_view('background','component/background',null,true);
          $this->template->write_view('side_nav','admin/sidebar',true);
          $this->template->write_view('content','admin/reward_success',true);
          $this->template->render();
       }
    }
    } else {
    	redirect('login', 'refresh');
    }
 }
 function success_reward(){
    $this->template->set_template('admin');
          $this->template->add_css('/assets/css/admin_sidenav.css');
          $this->template->write_view('main_nav','component/navbar',null,true);
          $this->template->write_view('background','component/background',null,true);
          $this->template->write_view('side_nav','admin/sidebar',true);
          $this->template->write_view('content','admin/reward_success',true);
          $this->template->render();
 }
 function cek_terdaftar($nim){
    $query_nim = $this->db->query("SELECT nim from mahasiswa WHERE nim = $nim");
    $array_nim = $query_nim->result_array();

    if(count($array_nim) == 1)
      return TRUE;
    else {
      
      return FALSE;
    }
  }

}
?>