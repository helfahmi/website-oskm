<?php
class Daftar extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function getData(){
	   $query = $this->db->query("SELECT * FROM divisi");
	   $data['success_status'] = false;
	   $data['numrow'] = $query->num_rows();
	   $data['div_option'] = $query->result_array();
	   $data['agama_option'] = array('', 'Islam', 'Kristen Katolik', 'Kristen Protestan', 'Buddha', 'Hindu', 'Konghucu');
	   return $data;
	}

	function index(){
redirect('home');
	   $this->load->helper(array('form'));
	   $this->load->helper('url');
	   $this->template->add_css('assets/css/timeline_page.css');
	   $this->template->write_view('main_nav','component/navbar',null);
	   $data = $this->getData();
	   $this->template->write_view('content','site/daftar',$this->getData());
	   //$this->template->write('background',');
	   $this->template->render();
	}


	function verify(){
		$succes_status=false;
		//This method will have the credentials validation
	   $this->load->library('form_validation');

	   $this->form_validation->set_message('is_natural', 'NIM dan No. HP harus berisi angka saja.');
	   $this->form_validation->set_message('is_unique', 'NIM yang Anda masukkan sudah terdaftar.');
	   $this->form_validation->set_message('pil_unik', 'Pilihan divisi yang Anda masukkan tidak unik (beda prioritas pilihan harus beda pilihan divisi).');
	   $this->form_validation->set_message('cek_agama', 'Anda harus memilih sebuah agama.');
	   $this->form_validation->set_message('exact_length', 'NIM harus berisi 8 angka.');
	   //set validasi 
	   $this->form_validation->set_rules('nama', 'Nama lengkap', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('nim', 'NIM', 'trim|required|xss_clean|is_unique[mahasiswa.nim]|is_natural|exact_length[8]');
	   $this->form_validation->set_rules('fakultas', 'Fakultas', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('asalsma', 'Asal SMA', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('hp', 'No. HP', 'trim|required|xss_clean|is_natural');
	   $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('penyakit', 'Penyakit', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
	   $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('agama', 'Agama', 'trim|required|xss_clean|callback_cek_agama');
	   
	   //jurusan bisa nggak ada
	   $this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|xss_clean'); 

	   $this->form_validation->set_rules('pil1', 'Pilihan divisi 1', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pil2', 'Pilihan divisi 2', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pil3', 'Pilihan divisi 3', 'trim|required|xss_clean|callback_pil_unik');
	   
	   //run validasi
	   if($this->form_validation->run() == FALSE){ 
	     //Field validation failed. 
	   	 // redirect('daftar/index');
	     // $this->load->view('daftar_view', $this->getData());
	   	 $this->template->write_view('main_nav','component/navbar',null);
	   	 $data = $this->getData();
	   	 $this->template->write_view('content','site/daftar',$this->getData());
	     $this->template->render();
	   } else {
	     //Field validation success. Insert data.
		 $query1 = "INSERT INTO mahasiswa(nama, nim, fakultas, asalsma, hp, alamat, penyakit, email, jenis_kelamin, agama) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	   	 $query2 = "INSERT INTO memilih(nim, pil1, pil2, pil3) VALUES (?, ?, ?, ?)";
	   	 $this->db->query($query1, array($this->input->post('nama'), $this->input->post('nim'), $this->input->post('fakultas'), $this->input->post('asalsma'), $this->input->post('hp'), $this->input->post('alamat'), $this->input->post('penyakit'), $this->input->post('email'), $this->input->post('jenis_kelamin'), $this->input->post('agama')));
	   	 $this->db->query($query2, array($this->input->post('nim'), $this->input->post('pil1'), $this->input->post('pil2'), $this->input->post('pil3')));
	   	 // $succes_status = true;
	   	 redirect('daftar/success','refresh');
	   }
	}

	function pil_unik($gakkepake){
		for($i=1;$i<=2;$i++){
			for($j=$i+1;$j<=3;$j++)
				if($this->input->post("pil$i") == $this->input->post("pil$j"))
					return false;
		}
		return true;
	}

	function cek_agama($agama){
		if($agama == '')
			return false;
		return true;
	}

	function success(){
		$this->template->write_view('main_nav','component/navbar',null);
	   	$this->template->write_view('content','success');
	    $this->template->render();
	}
	function cek_pernah_daftar($nim){
		$query =$this->db->where('nim',$nim)->from('memilih')->count_all_results();
		if($query>0){
			//artinya pernah daftar
			return false;
		}else{
			return true;
		}
	}
}
?>