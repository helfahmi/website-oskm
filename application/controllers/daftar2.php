<?php
class Daftar2 extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	private function getData(){
	   $query = $this->db->query("SELECT * FROM divisi");
	   $data['numrow'] = $query->num_rows();
	   $data['div_option'] = $query->result_array();
	   return $data;
	}

	function index(){
redirect('home');
	   $this->load->helper(array('form'));
	   $this->load->helper('url');
	   $this->load->view('daftar_view2', $this->getData());	
	}

	function verify(){
		//This method will have the credentials validation
	   $this->load->library('form_validation');

	   $this->form_validation->set_message('is_natural', 'NIM dan No. HP harus berisi angka saja.');
	   $this->form_validation->set_message('cek_terdaftar', 'NIM yang Anda masukkan belum terdaftar.');
	   $this->form_validation->set_message('pil_unik', 'Pilihan divisi yang Anda masukkan tidak unik.');
	   $this->form_validation->set_message('exact_length', 'NIM harus berisi 8 angka.');
	   //set validasi 
	   $this->form_validation->set_rules('nim', 'NIM', 'trim|required|xss_clean|callback_cek_terdaftar|is_natural|exact_length[8]');
	   
	   //jurusan bisa nggak ada
	   $this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|xss_clean|required'); 

	   $this->form_validation->set_rules('pil4', 'Pilihan divisi 1', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pil5', 'Pilihan divisi 2', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pil6', 'Pilihan divisi 3', 'trim|required|xss_clean|callback_pil_unik');
	   
	   //run validasi
	   if($this->form_validation->run() == FALSE){ 
	     //Field validation failed. 
	     $this->load->view('daftar_view2', $this->getData());
	   } else {
	     //Field validation success. Insert data.
	   	 $query1 = "UPDATE mahasiswa SET jurusan = ? WHERE nim = ?";
	   	 $query2 = "UPDATE memilih SET pil4 = ?, pil5 = ?, pil6 = ? WHERE nim = ?";
	   	 $this->db->query($query1, array($this->input->post('jurusan'), $this->input->post('nim')));
	   	 $this->db->query($query2, array($this->input->post('pil4'), $this->input->post('pil5'), $this->input->post('nim')));
	   	 $this->load->view('success');
	   }
	}

	function pil_unik($gakkepake){
		for($i=4;$i<=5;$i++){
			for($j=$i+1;$j<=6;$j++)
				if($this->input->post("pil$i") == $this->input->post("pil$j"))
					return false;
		}
		return true;
	}

	function cek_terdaftar($nim){
		$query_nim = $this->db->query("SELECT nim from mahasiswa WHERE nim = $nim");
		$array_nim = $query_nim->result_array();

		if(count($array_nim) == 1)
			return TRUE;
		else {
			
			return FALSE;
		}
	}

}
?>