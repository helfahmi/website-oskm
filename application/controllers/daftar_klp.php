<?php
class Daftar_Klp extends CI_Controller{
	public $jml_ang_min = 10;
	public $jml_ang = 25;
	public $count_ang;
	function __construct(){
		parent::__construct();
	}

	function index(){
	   $this->load->helper(array('form'));
	   $this->load->helper('url');
	   $data['jml_ang'] = $this->jml_ang;
	   $this->template->write_view('main_nav','component/navbar',null);
	   $this->template->write_view('content','site/daftar_view_klp',$data,true);
	   $this->template->render();
	   // $this->load->view('daftar_view_klp', $data);	
	}

	function verify(){
		//This method will have the credentials validation
	   $this->load->library('form_validation');

	   //set validasi 
	   $this->form_validation->set_message('cek_minimal', 'Minimal anggota yang terdaftar harus sebanyak '. $this->jml_ang_min.'.');
	   $this->form_validation->set_message('is_natural', 'NIM dan No. Kelompok harus berisi angka saja.');
	   $this->form_validation->set_message('cek_terdaftar', 'Salah satu NIM yang Anda masukkan belum terdaftar sebagai peserta.');
	   $this->form_validation->set_message('cek_unik', 'NIM yang Anda masukkan tidak unik.');
	   $this->form_validation->set_message('is_unique_nim', 'Salah satu NIM yang Anda masukkan sudah terdaftar sebagai anggota kelompok.');
	   //$this->form_validation->set_message('is_unique_klp', 'No. kelompok yang Anda masukkan sudah terdaftar sebagai sebuah kelompok.');

	   $this->count_ang = 0;
	   $valid_nims = array();
	   for($i=1;$i<=$this->jml_ang-1;$i++){
	   	  if($this->input->post('nim'.$i) != ''){	
	   		//input box ke $i terisi, tambah jumlah yg terisi, masukkan ke himpunan terisi, + set rule
	   		array_push($valid_nims, $i);
	   		$this->count_ang++;
	   		$this->form_validation->set_rules('nim'.$i, 'NIM Anggota ke-'.$i, 'trim|xss_clean|is_natural|callback_is_unique_nim|callback_cek_terdaftar');
	   	  } //kalo nggak keisi biarin dulu
	   }
	   
	   //validasi nim terakhir + set rule minimal anggota

	   //kalo kosong
	   if($this->input->post('nim'.$this->jml_ang) == ''){
	   		$this->form_validation->set_rules('nim'.$this->jml_ang, 'NIM Anggota ke-'.$i, 'callback_cek_minimal');
	   } else {
	   //ada isinya
	   		$this->form_validation->set_rules('nim'.$this->jml_ang, 'NIM Anggota ke-'.$i, 'trim|xss_clean|is_natural|callback_is_unique_nim|callback_cek_terdaftar|callback_cek_minimal');
	   }


	   //run validasi
	   if($this->form_validation->run() == FALSE){ 
	     //Field validation failed. User redirected to login page
	   	 $data['jml_ang'] = $this->jml_ang;
	   	 
	     $this->template->write_view('main_nav','component/navbar',null);
	     $this->template->write_view('content','site/daftar_view_klp',$data,true);
	     $this->template->render();

	   } else {
	     //Field validation success. Insert data.
		 $query = $this->db->query("SELECT * FROM kelompok ORDER BY no_klp DESC LIMIT 1"); 
		 $result = $query->row_array();
		 
		 if(count($result) == 0)
		 	$no_klp = 1;
		 else
		 	$no_klp = $result['no_klp'] + 1;


		 //hanya insert yg valid2 aja
		 foreach($valid_nims as $valid){
		 	$this->db->query("INSERT INTO kelompok VALUES ($no_klp, ".$this->input->post('nim'.$valid).")");
		 }
	   	 

	   	 //$this->db->query("INSERT INTO mahasiswa VALUES ('".$this->input->post('nama')."', ".$this->input->post('nim').",'".$this->input->post('fakultas')."',".$this->input->post('hp').",'".$this->input->post('alamat')."','".$this->input->post('penyakit')."','".$this->input->post('email')."','".$this->input->post('jenis_kelamin')."','".$this->input->post('agama')."','".$this->input->post('jurusan')."')");
	   	 //$this->db->query("INSERT INTO memilih VALUES (".$this->input->post('nim').", ".$this->input->post('pil1').",".$this->input->post('pil2').",".$this->input->post('pil3').")");
	   	 $data['no_klp'] = $no_klp;
	   	 $this->template->write_view('main_nav','component/navbar',null);
	     $this->template->write_view('content','site/success_klp',$data,true);
	     $this->template->render();
	   }
	}

	function success(){
		$data['no_klp'] = 2;
	   	$this->template->write_view('main_nav','component/navbar',null);
	    $this->template->write_view('content','site/success_klp',$data,true);
	    $this->template->render();
	}

	function cek_minimal($nim_terakhir){
		if($this->count_ang >= $this->jml_ang_min)
			return true;
		return false;
	}

	function cek_terdaftar($nim){
		$query_nim = $this->db->query("SELECT nim from mahasiswa WHERE nim = $nim");
		$array_nim = $query_nim->result_array();

		if(count($array_nim) == 1)
			return TRUE;
		else {
			
			return FALSE;
		}
	}

	function is_unique_nim($nim){
		$query_nim = $this->db->query("SELECT nim from kelompok WHERE nim = $nim");
		$array_nim = $query_nim->result_array();

		if(count($array_nim) == 1){
			
			return false;
		} else {
			return true;
		}
	}

	function is_unique_klp($klp){
		$query_nim = $this->db->query("SELECT no_klp from kelompok WHERE no_klp = $klp");
		$array_nim = $query_nim->result_array();

		if(count($array_nim) == 1){
			
			return false;
		} else {
			return true;
		}
	}

	function cek_unik($gak_kepake){
		for($i=1;$i<=$this->jml_ang;$i++){
			for($j=$i+1;$j<=$this->jml_ang;$j++){
				if($this->input->post("nim$i") == $this->input->post("nim$j")){
					return FALSE;
				}
			}
		}
		return TRUE;
	}
}
?>