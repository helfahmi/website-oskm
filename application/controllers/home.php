<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller{

	function __construct(){		
		parent::__construct();
		$this->load->library('template');
	}

	function index(){
		
		$this->template->set_template('home');
		// if we want to use template from william 
		$query = $this->db->query("SELECT * FROM posts LIMIT 5");
		$data['posts'] = $query->result_array();
		
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap.css');
		$this->template->add_css('assets/css/home_page.css'); // most important thing
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap-responsive.css');
		$this->template->add_css('assets/css/mod_carousel.css');
		$this->template->add_js('assets/lib/jquery/jquery.js');
		$this->template->add_js('assets/lib/jquery/jquery.scrollTo-1.4.3.1.min.js');
		$this->template->add_js('assets/lib/jquery/jquery.localscroll-1.2.7.js');
		$this->template->add_js('assets/lib/bootstrap/js/bootstrap.js');
		$this->template->write_view('main_nav','component/navbar',null,true);
		
		$this->template->write_view('side_nav','component/home_sidebar',$data,true);
		$this->template->write_view('background','component/background',null,true);
		
		$this->template->write_view('content','component/content',$data,true);
		
		$this->template->render();
		
		// $css_libs = $this->load->view('required/css_library_loader',NULL,True);
		// print($css_libs); DEBUGGING ONLY
		// $this->template->set('css_libs',$css_libs);
		// $this->template->load('template_fawwaz','required/js_library_loader');
		
		
		//ONE LINE CODE BELOW WORKS!
	//	$this->load->view('starter-template.php');
	}

	function gallery_manager(){
		$this->load->library('image_CRUD');
		$image_crud = new image_CRUD();

		
		$image_crud->set_table('gallery');
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('url');
		
		$image_crud->set_image_path('assets/img');
			
		$output = $image_crud->render();
		
		// $this->_example_output($output);
		$this->load->view('example_img.php',$output);	
	}


	/*
	* Gallery path url also included in database
	*
	*
	**/

	var $gallery_path;
	var $gallery_path_url;

	function gallery(){
		$this->gallery_path = realpath(APPPATH.'../assets/uploads');
		$this->gallery_path_url = upload_url();
		

		$files=scandir($this->gallery_path);
		$files=array_diff($files, array('.','..','files','index.html'));

		$images = array();
		$pattern = "/^[^(thumb)]/";
		foreach ($files as $file) {
			#if() if match the filter then ...
			// jika tidak cocok dengan pattern maka dibuang saja (abaikan)
			if(preg_match($pattern, $file)){
				$images [] = array(
					'file'=>$this->gallery_path_url.$file,
					'size'=>rand(1,3)
					);
			}
		}
		$data['images']	= $images;


		$this->template->set_template('home');
			

		$this->template->add_css('assets/lib/bootstrap/css/bootstrap.css');
		$this->template->add_css('assets/css/home_page.css'); // most important thing
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap-responsive.css');
		$this->template->add_css('assets/css/mod_carousel.css');

		// // Required js files
		// $gallery_page_js=$this->load->view('../../assets/js/gallery_page.js','',true);
		// $this->template->add_js($gallery_page_js,'embed');
		// $this->template->add_js('assets/lib/masonry-docs/masonry.pkgd.js');

		// $this->template->add_js('assets/lib/jquery/jquery.js');
		// $this->template->add_js('assets/lib/jquery/jquery.scrollTo-1.4.3.1.min.js');
		// $this->template->add_js('assets/lib/jquery/jquery.localscroll-1.2.7.js');
		// $this->template->add_js('assets/lib/bootstrap/js/bootstrap.js');
		

		// $this->template->write_view('main_nav','component/navbar',null,true);
		// // $this->template->write_view('side_nav','component/home_sidebar',$data,true);
		// // $this->template->write_view('background','component/background',null,true);

		// $this->template->write_view('content','site/gallery',$data,true);

		$this->template->set_template('gallery');
		$this->template->write_view('gallery_content','albums',$data);
		$this->template->write_view('main_nav','component/navbar',null,true);

		$this->template->render();










 	












		
		// $this->template->add_css('assets/css/gallery_page.css');
		// $this->template->add_css('assets/css/timeline_page.css');
		// $this->template->add_js('assets/lib/jquery/jquery.js');
		// $this->template->add_js('assets/lib/masonry-docs/masonry.pkgd.min.js');
		
		// // $my_wookmark_js=$this->load->view('../../assets/lib/wookmark/mywookmark.js','',true);
		// // $this->template->add_js($my_wookmark_js,'embed');	
		// // $wookmark_js=$this->load->view('../../assets/lib/wookmark/wookmark.js','',true);
		// // $this->template->add_js($wookmark_js,'embed');	
		
		// // $this->template->write_view('main_nav','component/navbar',null);
		

		// $this->template->write_view('content','site/gallery',$data);
		// $this->template->write_view('main_nav','component/navbar',null,true);


		// $this->template->write_view('background','component/background',null,true);
		
		
		// $this->template->render();
		// // $this->load->view();
		// // foreach ($images as $image) {
		// // 	echo $image.'<br/>';
		// // 	# code...
		// // }
	}

	function reward(){
		$this->template->set_template('reward');

		$this->template->add_css('assets/lib/bootstrap/css/bootstrap.css');
		$this->template->add_css('assets/css/home_page.css'); // most important thing
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap-responsive.css');
		$this->template->add_css('assets/css/mod_carousel.css');
		
		$this->template->write_view('main_nav','component/navbar',null,true);
		$this->template->render();
	}

	function single(){



		$this->template->set_template('single');



		//####################### BAGIAN GALLERY #######################
		$this->gallery_path = realpath(APPPATH.'../assets/uploads');
		$this->gallery_path_url = upload_url();
		

		$files=scandir($this->gallery_path);
		$files=array_diff($files, array('.','..','files','index.html'));

		$images = array();
		$pattern = "/^[^(thumb)]/";
		foreach ($files as $file) {
			#if() if match the filter then ...
			// jika tidak cocok dengan pattern maka dibuang saja (abaikan)
			if(preg_match($pattern, $file)){
				$images [] = array(
					'file'=>$this->gallery_path_url.$file,
					'size'=>rand(1,3)
					);
			}
		}
		$data['images']	= $images;

		$this->template->add_css('assets/lib/bootstrap/css/bootstrap.css');
		$this->template->add_css('assets/css/home_page.css'); // most important thing
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap-responsive.css');
		$this->template->add_css('assets/css/mod_carousel.css');
		$this->template->write_view('gallery','albums',$data);
		

		//####################### BAGIAN BERITA #######################
		// if we want to use template from william 
		$query = $this->db->query("SELECT * FROM posts LIMIT 5");
		$data['posts'] = $query->result_array();
		$this->template->write_view('news','component/news',$data,true);


		// Rendering
		$this->template->render();	
	}
}