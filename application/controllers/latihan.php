<?php 

class Latihan extends CI_Controller{

	function __construct(){		
		parent::__construct();
		$this->load->library('template');
	}

	function index(){
		
		// if we want to use template from william 
		$query = $this->db->query("SELECT * FROM posts");
		$data['posts'] = $query->result_array();
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap.css');
		$this->template->add_css('assets/css/mod_carousel.css'); // most important thing
		$this->template->add_css('assets/lib/bootstrap/css/bootstrap-responsive.css');
		$this->template->add_js('assets/lib/jquery/jquery.js');
		$this->template->add_js('assets/lib/bootstrap/js/bootstrap.js');
		$this->template->write_view('main_nav','component/navbar',null,true);
		$this->template->write_view('background','component/background',null,true);
		$this->template->write_view('content','home_view',$data,true);
		$this->template->render();
		// $css_libs = $this->load->view('required/css_library_loader',NULL,True);
		// print($css_libs); DEBUGGING ONLY
		// $this->template->set('css_libs',$css_libs);
		// $this->template->load('template_fawwaz','required/js_library_loader');
		
		
		//ONE LINE CODE BELOW WORKS!
	//	$this->load->view('starter-template.php');
	}
}




/* End of file temp_latihan.php */
/* Location: ./system/application/controllers/latihan.php */