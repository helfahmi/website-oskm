<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI

	class Login extends CI_Controller{
		function __construct(){
 			parent::__construct();
 		}

		public function index(){
		   $this->load->helper(array('form'));
		   $this->load->helper('url');
		   if($this->session->userdata('logged_in'))
		   	redirect('admin', 'refresh');
		   else

		   	// Modified by fawwaz
		   	$this->template->write_view('main_nav','component/navbar',null,true);
		   	$this->template->write_view('content','admin/login',null,true);
		   	$this->template->render();		   	
		}
	}
?>