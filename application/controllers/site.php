<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI

class Site extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Standard Libraries of codeigniter are required */
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('post_model');
		/* ------------------ */ 

		$this->load->library('grocery_CRUD');
	}
	function index(){
		
		$data['posts']=$this->post_model->get_posts();
		$this->template->write_view('content','site/posts',$data,true);
		$this->template->render();
	}
	function landing(){
		$this->load->view('landing');
	}
}