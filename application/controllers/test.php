<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Test extends CI_Controller{
 function __construct(){
    parent::__construct();
    /* Standard Libraries of codeigniter are required */
    $this->load->database();
    $this->load->helper('url');
    /* ------------------ */ 

    $this->load->library('grocery_CRUD');
 }

 function index(){
 }

 function manage(){
	$this->grocery_crud->set_subject('Mahasiswa');
    $this->grocery_crud->set_table('mahasiswa');
    
    $output = $this->grocery_crud->render();

    $this->_example_output($output);
 }

 function _example_output($output = null){
  $this->load->view('templates/grocery_template.php',$output);    
 }
}

?>