<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI

class Timeline extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Standard Libraries of codeigniter are required */
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('post_model');
		/* ------------------ */ 

		$this->load->library('grocery_CRUD');
	}
	function index(){
		$this->template->add_css('assets/css/timeline_page.css');
		$this->template->write_view('main_nav','component/navbar',null,true);
		$this->template->write_view('content','component/timeline',null,true);
		$this->template->render();
	}
}