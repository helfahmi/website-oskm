<?php
class Post_model extends CI_Model{

	var $tbl_name='posts';

	function __construct(){
		parent::__construct();
	}

	function get_posts(){
		$query=$this->db->order_by("date","desc")->get($this->tbl_name);
		return $query->result_array();
	}
}

/* End of file post_model.php */ 
/* Location: ./application/models/post_model.php */