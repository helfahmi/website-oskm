<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>

<!-- Place this in the body of the page content -->

<?php echo validation_errors(); ?>
<?php echo form_open($action); ?>
	<input type="text" name="title" value="<?php echo $title; ?>">
    <textarea name="content"><?php echo $content; ?></textarea>
    <input type="submit" value="Submit">
</form>