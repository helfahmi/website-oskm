<?php if($message != "") echo "[$message]<br/>" ?>
<table class="table table-bordered table-striped">
<thead><tr>
		<th><b>Title</b></th>
		<th><b>Author</b></th>
		<th><b>Post Date</b></th>
		<th><b>Delete Post</b></th>
		<th><b>Edit Post</b></th>
		<th><b>View</b></th>
</tr></thead>
<tbody>
<?php foreach ($posts as $post): ?>
	<tr>
		<td><?php echo $post['title'] ?></td>
		<td><?php echo $post['author'] ?></td>
		<td><?php echo $post['date'] ?></td>
		<td><?php echo "<a href=\"/admin/delete/". $post['id'] . " \">[Delete]</a>"; ?></td>
		<td><?php echo "<a href=\"/admin/edit/". $post['id'] . " \">[Edit]</a>"; ?></td>
		<td><?php echo "<a href=\"/page/view/". $post['id'] . " \">[View]</a>"; ?></td>
	</tr>
<?php endforeach ?>
</tbody>
</table>