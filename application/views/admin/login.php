
<!--Added inline style inorder to simplicity-->
<style type="text/css">
	.login_box{
		position: absolute;
		top: 25%;
	}
</style>
<div class="row login_box">
<div class="span4 offset4">
<div class="well">
<legend>Sign in for admin page</legend>
<?php echo form_open('verifylogin'); ?>	
<?php if(validation_errors()!=false){ ?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert" href="#">x</a><?php echo validation_errors(); ?>
</div>
<?php }?>
<input class="span3" placeholder="Username" type="text" name="username">
<input class="span3" placeholder="Password" type="password" name="password">
<label class="checkbox">
<input type="checkbox" name="remember" value="1"> Remember Me
</label>
<button class="btn-info btn" type="submit">Login</button>
</form>
</div>
</div>
</div>