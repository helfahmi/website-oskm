<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title>OSKM 2013</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="author" content="">   
   <!--<script src="<?php echo lib_url().'jquery/jquery.js';?>"></script>-->
   <link href="<?php echo lib_url().'bootstrap/css/bootstrap.css'?>" rel="stylesheet">
   <link href="<?php echo lib_url().'bootstrap/css/bootstrap-responsive.css'; ?>" rel="stylesheet">
   <script src="<?php echo lib_url().'jquery/jquery.js'; ?>"></script>
   <script src="<?php echo lib_url().'bootstrap/js/bootstrap.js'; ?>"></script>
   <script type="text/javascript">
      // $(document).ready(function() {

        // $('.carousel').content;
        // carousel({interval: 4500});
      // });
    </script>    
    <!--<script src="../../../assets/lib/bootstrap/js/bootstrap.js"></script>-->
   
   <?= $_scripts ?>
   <?= $_styles ?>   
</head>
<body>
   <div class="container">
   <?php print $main_nav;?>
   <?php print $background;?>
   <?php print $content; ?>
   <?php print $footer; ?>
   </div>   
</body>
</html>






/////====================
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="<?php echo lib_url().'jquery/jquery.js'; ?>"></script>
    <!-- Le styles -->
    <link href="<?php echo lib_url().'bootstrap/css/bootstrap.css'?>" rel="stylesheet">
    <!--<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }

    </style>-->
    <script type="text/javascript">
      // $(document).ready(function() {
      //   $('.carousel').carousel({interval: 4500});
      // });
    </script>
    <style>
      .carousel { z-index: -99; }
      .carousel .item {
        position: fixed; 
        width: 100%; height: 100%;
        -webkit-transition: opacity 1s  ;
        -moz-transition: opacity 1s;
        -ms-transition: opacity 1s;
        -o-transition: opacity 1s;
        transition: opacity 1s;

      }
      .carousel .one {
        background: url(img/food1.jpg);
        background-size: cover;
        -moz-background-size: cover;
      }
      .carousel .two {
        background: url(img/food2.jpg);
        background-size: cover;
        -moz-background-size: cover;
      }
      .carousel .three {
        background: url(img/food3.jpg);
        background-size: cover;
        -moz-background-size: cover;
      }
      .carousel .active.left {
        left:0;
        opacity:0;
        z-index:2;
      }
      </style>
    <link href="<?php echo lib_url().'bootstrap/css/bootstrap-responsive.css'; ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Project name</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div id="myCarousel" class="carousel container slide">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item one"></div>
                    <div class="item two active"></div>
                    <div class="item three"></div>
                </div>
      </div>
      <h1>Bootstrap starter template</h1>
      <p>Use this document as a way to quick start any new project.<br> All you get is this message and a barebones HTML document.</p>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo lib_url().'jquery/jquery.js'; ?>"></script>
   <script src="<?php echo lib_url().'bootstrap/js/bootstrap.js'; ?>"></script>
    <!-- <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script> -->

  </body>
</html>
