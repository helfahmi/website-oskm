    <!DOCTYPE HTML>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>OSKM2013 - #UntukIndonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Styles -->
    <link href="<?php echo lib_url()."legenda/";?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo lib_url()."legenda/";?>css/style.css" rel="stylesheet">
    <link rel='stylesheet' id='prettyphoto-css'  href="<?php echo lib_url()."legenda/";?>css/prettyPhoto.css" type='text/css' media='all'>
    <link href="<?php echo lib_url()."legenda/";?>css/fontello.css" type="text/css" rel="stylesheet">
    <!--###### Style gallery ######-->
    <!-- colorbox styles -->
  <link rel="stylesheet" href="<?php echo lib_url();?>wookmark2/example-lightbox/css/colorbox.css">
  <!-- Styling for your grid blocks -->
  <link rel="stylesheet" href="<?php echo lib_url();?>wookmark2/example-lightbox/css/style.css">
    <!--###### End Style Gallery ###### -->
    <!--[if lt IE 7]>
            <link href="<?php echo lib_url()."legenda/";?>css/fontello-ie7.css" type="text/css" rel="stylesheet">  
        <![endif]-->
    <!-- Google Web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <style>
    body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
    }
    </style>
    <link href="<?php echo lib_url()."legenda/";?>css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
          <script src="<?php echo lib_url()."legenda/";?>http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo lib_url()."legenda/";?>img/favicon.ico">
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/jquery.js"></script>
    <!-- Load ScrollTo -->
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/jquery.scrollTo-1.4.2-min.js"></script>
    <!-- Load LocalScroll -->
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/jquery.localscroll-1.2.7-min.js"></script>
    <!-- prettyPhoto Initialization -->
    <script type="text/javascript" charset="utf-8">
          $(document).ready(function(){
            $("a[rel^='prettyPhoto']").prettyPhoto();
          });
        </script>
    </head>
    <body>
    <!--******************** NAVBAR ********************-->
    <div class="navbar-wrapper">
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
            <h1 class="brand"><a href="<?php echo lib_url()."legenda/";?>#top">OSKM ITB 2013</a></h1>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <nav class="pull-right nav-collapse collapse">
              <ul id="menu-main" class="nav">
                <li><a title="portfolio" href="#portfolio">Home</a></li>
                <li><a title="services" href="#services">Berita</a></li>
                <li><a title="news" href="#news">Galery</a></li>
                <li><a title="team" href="#team">Team</a></li>
                <li><a title="team" href="#timeline">Timeline</a></li>
                <li><a title="contact" href="#contact">Contact</a></li>
              </ul>
            </nav>
          </div>
          <!-- /.container -->
        </div>
        <!-- /.navbar-inner -->
      </div>
      <!-- /.navbar -->
    </div>
    <!-- /.navbar-wrapper -->
    <div id="top"></div>
    <!-- ******************** HeaderWrap ********************-->
    <div id="headerwrap">
      <header class="clearfix">
        <h1>OSKM 2013 ! <span>#UntukIndonesia!</span></h1>
        <div class="container">
          <div class="row">
            <div class="span12"><br><br><br><br><br><br>
              <!-- <h2>Signup for our Newsletter to be updated</h2>
              <input type="text" name="your-email" placeholder="you@yourmail.com" class="cform-text" size="40" title="your email">
              <input type="submit" value="Notify me" class="cform-submit"> -->
            </div>
          </div>
          <div class="row">
            <div class="span12">
              <ul class="icon">
                <li><a href="http://pinterest.com/oskm2013/boards" target="_blank"><i class="icon-pinterest-circled"></i></a></li>
                <li><a href="http://facebook.com/oskm2013" target="_blank"><i class="icon-facebook-circled"></i></a></li>
                <li><a href="http://twitter.com/oskm2013" target="_blank"><i class="icon-twitter-circled"></i></a></li>
                <!-- <li><a href="http://oskm2013" target="_blank"><i class="icon-gplus-circled"></i></a></li>
                <li><a href="http://oskm2013" target="_blank"><i class="icon-skype-circled"></i></a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </header>
    </div>
    <!--******************** Feature ********************-->
    <div class="scrollblock">
      <section id="feature">
        <div class="container">
          <div class="row">
            <div class="span12">
              <article>
                <p>Saya harap dengan OSKM 2013, mahasiswa bisa lebih</p>
                <p>Cerdas dan bijak dengan nilai nilai kearifan lokal.</p>
                <p>-Risky Adha, Ketua OSKM, SBM '13</p>
              </article>
            </div>
            <!-- ./span12 -->
          </div>
          <!-- .row -->
        </div>
        <!-- ./container -->
      </section>
    </div>
    <hr>
    <!--******************** Timeline Section ********************-->
    <section id="timeline" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-cog-circled"></i></div>
        <h1>Timeline OSKM</h1>
        <div class="row">
          <div class="timeline_container">            
            <div id="main_gallery" role="main">

              <?php print $gallery;?>

            </div>
          </div>
        </div>
      </div>
    </section>
    <!--******************** Gallery Section ********************-->
    <section id="gallery" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-cog-circled"></i></div>
        <h1>Gallery OSKM</h1>
        <div class="row">
          <div class="gallery_container">
          <iframe src='http://embed.verite.co/timeline/?source=0Aqq9bwEtFm49dHEtdnpxR1ZyVXh4cUlJckh0U1M3Qmc&font=DroidSerif-DroidSans&maptype=toner&lang=en&hash_bookmark=true&height=650' width='100%' height='650' frameborder='0'></iframe>
          </div>
        </div>
      </div>
    </section>
    <!--******************** Services Section ********************-->
    <section id="services" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-cog-circled"></i></div>
        <h1>Informasi umum OSKM</h1>
        <!-- Four columns -->
        <div class="row">
          <div class="span3">
            <div class="align"> <i class="icon-desktop sev_icon"></i> </div>
            <h2>Sambutan OSKM</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
          <!-- /.span3 -->
          <div class="span3">
            <div class="align"> <i class="icon-vector sev_icon"></i> </div>
            <h2>Sejarah OSKM</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
          <!-- /.span3 -->
          <div class="span3">
            <div class="align"> <i class="icon-basket sev_icon"></i> </div>
            <h2>Apa itu OSKM</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
          <!-- /.span3 -->
          <div class="span3">
            <div class="align"> <i class="icon-mobile-1 sev_icon"></i> </div>
            <h2>Visi misi</h2>
            <p>1. Menumbuhkan rasa cinta tanah air #visimisiOSKM.2. Membentuk mahasiswa yang kreatif, konstruktif, dan kritis #visimisiOSKM.3. Menanamkan pengetahuan urgensi berkemahasiswaan ITB #visimisiOSKM</p>
          </div>
          <!-- /.span3 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </section>
    <hr>

    <!--******************** Portfolio Section ********************-->
    <section id="portfolio" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-desktop-circled"></i></div>
        <h1 id="folio-headline">Berita</h1>

        <?php echo $news; ?>
        
        <div class="row">
          <div class="span4">
            <div class="mask2"> <a href="<?php echo lib_url()."legenda/";?>img/portfolio-01.jpg" rel="prettyPhoto"><img src="<?php echo lib_url()."legenda/";?>img/portfolio-03.jpg" alt=""></a> </div>
            <div class="inside">
              <hgroup>
                <h2>Ethan Marcotte Design</h2>
              </hgroup>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <a class="more-link" href="<?php echo lib_url()."legenda/";?>#">view project</a> </div>
            </div>
            <!-- /.inside -->
          </div>
          <!-- /.span4 -->
          <div class="span4">
            <div class="mask2"> <a href="<?php echo lib_url()."legenda/";?>img/portfolio-02.jpg" rel="prettyPhoto"><img src="<?php echo lib_url()."legenda/";?>img/portfolio-01.jpg" alt=""></a> </div>
            <div class="inside">
              <hgroup>
                <h2>A Book Apart</h2>
              </hgroup>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <a class="more-link" href="<?php echo lib_url()."legenda/";?>#">view project</a> </div>
            </div>
            <!-- /.inside -->
          </div>
          <!-- /.span4 -->
          <div class="span4">
            <div class="mask2"> <a href="<?php echo lib_url()."legenda/";?>img/portfolio-03.jpg" rel="prettyPhoto"><img src="<?php echo lib_url()."legenda/";?>img/portfolio-02.jpg" alt=""></a> </div>
            <div class="inside">
              <hgroup>
                <h2>Four Rules for Designers</h2>
              </hgroup>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <a class="more-link" href="<?php echo lib_url()."legenda/";?>#">view project</a> </div>
            </div>
            <!-- /.inside -->
          </div>
          <!-- /.span4 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </section>
    <hr>
    
    <!--******************** Testimonials Section ********************-->
    <section id="testimonials" class="single-page hidden-phone">
      <div class="container">
        <div class="row">
          <div class="blockquote-wrapper">
            <blockquote class="mega">
              <div class="span4">
                <p class="cite">John Doe &amp; Sons:</p>
              </div>
              <div class="span8">
                <p class="alignright">"We highly appreciate the enthusiasm and creative talent of the people at Legend!"</p>
              </div>
            </blockquote>
          </div>
          <!-- /.blockquote-wrapper -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </section>
    <hr>
    <!--******************** News Section ********************-->
    <section id="news" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-pencil-circled"></i></div>
        <h1>Gallery</h1>
        <!-- Three columns -->
        <div class="row">
          <article class="span4 post"> <img class="img-news" src="<?php echo lib_url()."legenda/";?>img/blog_img-01.jpg" alt="">
            <div class="inside">
              <p class="post-date"><i class="icon-calendar"></i> March 17, 2013</p>
              <h2>A girl running on a road</h2>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. &hellip;</p>
                <a href="<?php echo lib_url()."legenda/";?>#" class="more-link">read more</a> </div>
            </div>
            <!-- /.inside -->
          </article>
          <!-- /.span4 -->
          <article class="span4 post"> <img class="img-news" src="<?php echo lib_url()."legenda/";?>img/blog_img-02.jpg" alt="">
            <div class="inside">
              <p class="post-date">February 28, 2013</p>
              <h2>A bear sleeping on a tree</h2>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. &hellip;</p>
                <a href="<?php echo lib_url()."legenda/";?>#" class="more-link">read more</a> </div>
            </div>
            <!-- /.inside -->
          </article>
          <!-- /.span4 -->
          <article class="span4 post"> <img class="img-news" src="<?php echo lib_url()."legenda/";?>img/blog_img-03.jpg" alt="">
            <div class="inside">
              <p class="post-date">February 06, 2013</p>
              <h2>A Panda playing with his baby</h2>
              <div class="entry-content">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. &hellip;</p>
                <a href="<?php echo lib_url()."legenda/";?>#" class="more-link">read more</a> </div>
            </div>
            <!-- /.inside -->
          </article>
          <!-- /.span4 -->
        </div>
        <!-- /.row -->
        <a href="<?php echo lib_url()."legenda/";?>#" class="btn btn-large">Go to our blog</a> </div>
      <!-- /.container -->
    </section>
    <hr>
    <!--******************** Team Section ********************-->
    <section id="team" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-group-circled"></i></div>
        <h1>Meet the team</h1>
        <!-- Five columns -->
        <div class="row">
          <div class="span2 offset1">
            <div class="teamalign"> <img class="team-thumb img-circle" src="<?php echo lib_url()."legenda/";?>img/portrait-1.jpg" alt=""> </div>
            <h3>Andrew</h3>
            <div class="job-position">web designer</div>
          </div>
          <!-- ./span2 -->
          <div class="span2">
            <div class="teamalign"> <img class="team-thumb img-circle" src="<?php echo lib_url()."legenda/";?>img/portrait-2.jpg" alt=""> </div>
            <h3>Stephen</h3>
            <div class="job-position">web developer</div>
          </div>
          <!-- ./span2 -->
          <div class="span2">
            <div class="teamalign"> <img class="team-thumb img-circle" src="<?php echo lib_url()."legenda/";?>img/portrait-3.jpg" alt=""> </div>
            <h3>Maria</h3>
            <div class="job-position">graphic designer</div>
          </div>
          <!-- ./span2 -->
          <div class="span2">
            <div class="teamalign"> <img class="team-thumb img-circle" src="<?php echo lib_url()."legenda/";?>img/portrait-4.jpg" alt=""> </div>
            <h3>John</h3>
            <div class="job-position">project manager</div>
          </div>
          <!-- ./span2 -->
          <div class="span2">
            <div class="teamalign"> <img class="team-thumb img-circle" src="<?php echo lib_url()."legenda/";?>img/portrait-2.jpg" alt=""> </div>
            <h3>Ashton</h3>
            <div class="job-position">real owner</div>
          </div>
          <!-- ./span2 -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="span10 offset1">
            <hr class="featurette-divider">
            <div class="featurette">
              <h2 class="featurette-heading">Want to know more? <span class="muted">| a little about us</span></h2>
              <p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores.</p>
              <p>At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</p>
              <p>A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.</p>
            </div>
            <!-- /.featurette -->
            <hr class="featurette-divider">
          </div>
          <!-- .span10 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </section>
    <!--******************** Contact Section ********************-->
    <section id="contact" class="single-page scrollblock">
      <div class="container">
        <div class="align"><i class="icon-mail-2"></i></div>
        <h1>Contact us now!</h1>
        <div class="row">
          <div class="span12">
            <div class="cform" id="theme-form">
              <form action="#" method="post" class="cform-form">
                <div class="row">
                  <div class="span6"> <span class="your-name">
                    <input type="text" name="your-name" placeholder="Your Name" class="cform-text" size="40" title="your name">
                    </span> </div>
                  <div class="span6"> <span class="your-email">
                    <input type="text" name="your-email" placeholder="Your Email" class="cform-text" size="40" title="your email">
                    </span> </div>
                </div>
                <div class="row">
                  <div class="span6"> <span class="company">
                    <input type="text" name="company" placeholder="Your Company" class="cform-text" size="40" title="company">
                    </span> </div>
                  <div class="span6"> <span class="website">
                    <input type="text" name="website" placeholder="Your Website" class="cform-text" size="40" title="website">
                    </span> </div>
                </div>
                <div class="row">
                  <div class="span12"> <span class="message">
                    <textarea name="message" class="cform-textarea" cols="40" rows="10" title="drop us a line."></textarea>
                    </span> </div>
                </div>
                <div>
                  <input type="submit" value="Send message" class="cform-submit pull-left">
                </div>
                <div class="cform-response-output"></div>
              </form>
            </div>
          </div>
          <!-- ./span12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </section>
    <hr>
    <div class="footer-wrapper">
      <div class="container">
        <footer>
          <small>&copy; 2013 Inbetwin Network. All rights reserved.</small>
        </footer>
      </div>
      <!-- ./container -->
    </div>
    <!-- Loading the javaScript at the end of the page -->
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="<?php echo lib_url()."legenda/";?>js/site.js"></script>
    
    <!-- ***** Needed for gallery section **** -->
    <!-- Include the imagesLoaded plug-in -->
    <script src="<?php echo lib_url();?>wookmark2/libs/jquery.imagesloaded.js"></script>

    <!-- include colorbox -->
    <script src="<?php echo lib_url();?>wookmark2/libs/jquery.colorbox-min.js"></script>

    <!-- Include the plug-in -->
    <script src="<?php echo lib_url();?>wookmark2/jquery.wookmark.js"></script>

    <!-- Once the page is loaded, initalize the plug-in. -->
    <script type="text/javascript">
      (function ($){
        $('#tiles').imagesLoaded(function() {
          // Prepare layout options.
          var options = {
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $('#main_gallery'), // Optional, used for some extra CSS styling
            offset: 2, // Optional, the distance between grid items
            itemWidth: 210 // Optional, the width of a grid item
          };

          // Get a reference to your grid items.
          var handler = $('#tiles li');

          // Call the layout function.
          handler.wookmark(options);

          // Init lightbox
          $('a', handler).colorbox({
            rel: 'lightbox'
          });
        });
      })(jQuery);
    </script>
    
	</script>
    </body>
    </html>
