    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand visit" href="<?php echo base_url(); ?>">oskm itb 2013</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="<?php echo base_url(); ?>"><i class="icon-fixed-width icon-home icon-large"></i></a></li>
              <li><a href="<?php echo base_url(); ?>"><i class="icon-fixed-width icon-comment icon-large"></i></a></li>
              <li><a href="<?php echo site_url().'timeline'; ?>"><i class="icon-fixed-width icon-calendar-empty icon-large"></i></a></li>
<!--              <li><a href="<?php echo site_url().'daftar'?>"><i class="icon-fixed-width icon-user icon-large"></i></a></li>
              <li><a href="<?php echo site_url().'daftar_klp'?>"><i class="icon-fixed-width icon-group icon-large"></i></a></li>-->
              <li><a href="#"><i class="icon-fixed-width icon-camera-retro icon-large"></i>Dokumentasi</a></li>
              <li><a href="#"><i class="icon-fixed-width icon-trophy icon-large"></i>Reward</a></li>
              <li><a href="#"><i class="icon-fixed-width icon-check icon-large"></i>Tugas</a></li>
            </ul>
            <ul class="nav">
              <li><a href="http://www.facebook.com/groups/158193437695015/"><i class="icon-fixed-width icon-facebook icon-large"></i></a></li>
              <li><a href="https://twitter.com/oskm2013"><i class="icon-fixed-width icon-twitter icon-large"></i></a></li>
              <li><a href="#"><i class="icon-fixed-width icon-pinterest icon-large"></i></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>