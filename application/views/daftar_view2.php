<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <title>OSKM 2013</title>
 </head>
 <body>
<h1>Daftar Baru OSKM 2013 Gelombang 2</h1>
  <?php echo validation_errors(); ?>
  <?php echo form_open('daftar2/verify'); ?>
    <table>
     <tr>
         <td><label for="nim">NIM:</label></td>
         <td><input type="text" size="20" id="nim" value="<?php echo set_value('nim'); ?>" name="nim"/></td>
     </tr>
     <tr>
         <td><label for="jurusan">Jurusan:</label></td>
         <td><input type="text" size="20" id="jurusan" value="<?php echo set_value('jurusan'); ?>" name="jurusan"/></td>
     </tr>
     <tr>
         <td><label for="pil4">Pilihan Divisi 1:</label></td>
         <td>
            <select name="pil4">
                <?php
                    foreach($div_option as $opt){
                        echo "<option value=".$opt['id'].">". $opt['nama'] ."</option>";
                    }
                ?>
            </select>
         </td>
     </tr>
     <tr>
         <td><label for="pil5">Pilihan Divisi 2:</label></td>
         <td>
            <select name="pil5">
                <?php
                    foreach($div_option as $opt){
                        echo "<option value=".$opt['id'].">". $opt['nama'] ."</option>";
                    }
                ?>
            </select>
         </td>
     </tr>
     <tr>
         <td><label for="pil6">Pilihan Divisi 3:</label></td>
         <td>
            <select name="pil6">
                <?php
                    foreach($div_option as $opt){
                        echo "<option value=".$opt['id'].">". $opt['nama'] ."</option>";
                    }
                ?>
            </select>
         </td>
     </tr>
     <tr>
        <td>
        <input type="submit" value="Daftar"/>
        </td>
     </tr>
    </table> 
   </form>
 </body>
</html>