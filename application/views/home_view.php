	<link rel="stylesheet" href="assets/css/scrolldeck/style.css" type="text/css">
	
	<script src="assets/js/jquery-1.8.2.min.js"></script>
	<script src="assets/js/jquery.scrollTo-1.4.3.1.min.js"></script>
	<script src="assets/js/jquery.scrollorama.js"></script>
	<script src="assets/js/jquery.easing.1.3.js"></script>
	<script src="assets/js/jquery.scrolldeck.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
		  var deck = new $.scrolldeck();
		});
	</script>

<body>
<?php 
	$i = 1;
	foreach($posts as $post){
		echo "<div class=\"slide\" id=\"post$i\">";
		echo "<p>";
		echo "<h2>".$post['title']."</h2>";
		echo $post['date'] ." - by ". $post['author'];
		echo $post['content'];
		echo "</p>";
		echo "</div>";
	}
?>
</body>