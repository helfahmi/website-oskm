<!DOCTYPE html>
<html lang="en">
    <head>
        <title>OSKM ITB 2013</title>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Splash and Coming Soon Page Effects with CSS3" />
        <meta name="keywords" content="coming soon, splash page, css3, animation, effect, web design" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="<?php echo css_url().'reset2.css'; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo css_url().'landing_page2.css'; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo css_url().'landing_page.css'; ?>" />
		<!--[if lt IE 10]>
				<link rel="stylesheet" type="text/css" href="css/style3IE.css" />
		<![endif]-->

    </head>
    <body>
<div id="bg_image"></div>
        <div class="container" style="background:url(<?php echo img_url().'OSKM.png';?>) no-repeat bottom left fixed">
            <!-- <div class="header">
                <a href="http://tympanus.net/Tutorials/LateralOnScrollSliding/">
                    <strong>&laquo; Previous Demo: </strong>Lateral On-Scroll Sliding with jQuery
                </a>
                <span class="right">
                    <a href="http://tympanus.net/codrops/2011/12/07/splash-and-coming-soon-page-effects-with-css3/">
                        <strong>Back to the Codrops Article</strong>
                    </a>
                </span>
                <div class="clr"></div> -->
        </div>
        <!-- <h1 class="main">Splash &amp; Coming Soon Page Effects with CSS3</h1>
			<p class="demos">
				<a href="index.html">Demo 1</a>
				<a href="index2.html">Demo 2</a>
				<a class="current-demo" href="index3.html">Demo 3</a>
			</p> -->
		<div class="sp-container">
				<div class="sp-content">
					<div class="sp-wrap sp-left">
						<h2>
							<span class="sp-top">
							<br/><br/>
							</span> 
							<span class="sp-mid">OSKM</span> 
							<span class="sp-bottom"><br/></span>
						</h2>
					</div>
					<div class="sp-wrap sp-right">
						<h2>
							<span class="sp-top">Wouldn't that be absolutely</span> 
							<span class="sp-mid">Asik<i>!</i><i>?</i></span> 
							<span class="sp-bottom">Yeah, it would!</span> 
						</h2>
					</div>
				</div>
				<div class="sp-full">
					<h2>OSKM ITB 2013 #UntukIndonesia!</h2>
					<a href="<?php echo site_url().'daftar'?>">Daftar disini!</a>
				</div>
			</div>
        </div>
    </body>
</html>