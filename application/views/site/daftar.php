<div style="position:relative; top: 50px;" class="container">
<div class="row-fluid">
<legend>Daftar OSKM 2013</legend>
    <?php 
    if (validation_errors()){
        echo  "<div class=\"container\">
            <div class=\"alert alert-error\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                <strong>Oops !</strong>".validation_errors()."</div></div>";
        } ?>
  <?php 
    if ($success_status){
        echo  "<div class=\"container\">
            <div class=\"alert alert-success\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                <strong>Succes!</strong> Data anda telah dimasukan
            </div>
          </div>";
  } ?>
  <?php echo form_open('daftar/verify',array('class'=>'form-horizontal')); ?>
  <fieldset>

        <div class="span6">
        <!--Nama -->
        <div class="control-group">
         <label for="nama" class="control-label">Nama Lengkap</label>
            <div class="controls">
                <p class="help-block">Silahkan isi dengan nama lengkap</p>
                <input placeholder="Nama" class="input-xlarge" type="text" size="20" id="nama" value="<?php echo set_value('nama'); ?>" name="nama"/>
            </div>
        </div>
        <!--NIM -->
        <div class="control-group">
         <label for="nim" class="control-label">NIM</label>
            <div class="controls">
                <p class="help-block">Gunakan nim TPB saja</p>
                <input placeholder="NIM" class="input-xlarge" type="text" size="20" id="nim" value="<?php echo set_value('nim'); ?>" name="nim"/>
            </div>
        </div>
        <!--Fakultas -->
        <div class="control-group">
         <label for="fakultas" class="control-label">Fakultas</label>
            <div class="controls">
                <p class="help-block">Singkatan fakultasnya saja ex: FMIPA, bukan Fakultas Matematika dan IPA</p>
                <input placeholder="Fakultas" class="input-xlarge" type="text" size="20" id="fakultas" value="<?php echo set_value('fakultas'); ?>" name="fakultas"/>
            </div>
        </div>
        <!--A-->
        <div class="control-group">
         <label for="fakultas" class="control-label">Asal SMA</label>
            <div class="controls">
                <input placeholder="Asal SMA" class="input-xlarge" type="text" size="20" id="asalsma" value="<?php echo set_value('asalsma'); ?>" name="asalsma"/>
            </div>
        </div>

        <!--HP -->
        <div class="control-group">
            <label for="hp" class="control-label">No. HP</label>
            <div class="controls">
                 <div class="input-prepend">
                    <input type="text" id="hp" placeholder="No. HP"value="<?php echo set_value('hp'); ?>" name="hp"/>
                </div>
            </div>
        </div>
        
        <!--Alamat -->
        <div class="control-group">
         <label for="alamat" class="control-label">Alamat</label>
            <div class="controls">
                <p class="help-block">Alamat rumah bagi yang tinggal dari bandung/Alamat kos-an bagi yang ngekos</p>
                <input placeholder="Alamat" class="input-xlarge" type="text" size="20" id="alamat" value="<?php echo set_value('alamat'); ?>" name="alamat"/>
            </div>
        </div>

        <!--Penyakit -->
        <div class="control-group">
         <label for="penyakit" class="control-label">Penyakit</label>
            <div class="controls">
                <p class="help-block">Silahkan dilengkapi pisahkan dengan koma jika lebih dari satu</p>
                <input placeholder="Penyakit" class="input-xlarge" type="text" size="20" id="penyakit" value="<?php echo set_value('penyakit'); ?>" name="penyakit"/>
            </div>
        </div>


        </div>
        <div class="span6">
        <!--Email -->
        <div class="control-group">
         <label for="email" class="control-label">Email</label>
            <div class="controls">
                <p class="help-block"></p>
                <input placeholder="Email" class="input-xlarge" type="text" size="20" id="email" value="<?php echo set_value('email'); ?>" name="email"/>
            </div>
        </div>
        <!-- jenis kelamin-->
        <div class="control-group">
         <label for="jenis_kelamin" class="control-label">Jenis Kelamin</label>
            <div class="controls">
                <p class="help-block"></p>
                <select id="country" name="jenis_kelamin" class="input-xlarge">
                    <option selected="selected">Pria</option>
                    <option>Wanita</option>
                </select>
            </div>
        </div>

        <div class="control-group">
         <label for="agama" class="control-label">Agama</label>
            <div class="controls">
                <p class="help-block"></p>
                <select id="country" name="agama" class="input-xlarge">
                    <?php       
                        foreach($agama_option as $opt){
                            echo "<option value='".$opt."'";
                            if($opt == '')
                                echo set_select('agama', $opt, true);
                            else
                                echo set_select('agama', $opt);
                            echo ">". $opt ."</option>\n\t";
                        }
                    ?>
                </select>
            </div>
        </div>
        
        <div class="control-group">
         <label for="pil1" class="control-label">Prioritas Pilihan Divisi 1</label>
            <div class="controls">
                <p class="help-block">Pilih divisi 1. Pilihan 1-3 harus berbeda-beda</p>
                <select id="country" name="pil1" class="input-xlarge">
                        <?php
                            $first = true;
                            foreach($div_option as $opt){
                                echo "<option value=".$opt['id'];
                                if($first){
                                    echo set_select('pil1', $opt['id'], true);
                                    $first = false;
                                } else {
                                    echo set_select('pil1', $opt['id']);
                                }
                                echo ">". $opt['nama'] ."</option>";
                            }
                        ?>
                </select>
            </div>
        </div>

        <div class="control-group">
         <label for="pil2" class="control-label">Prioritas Pilihan Divisi 2</label>
            <div class="controls">
                <p class="help-block">Pilih divisi 2.</p>
                <select id="country" name="pil2" class="input-xlarge">
                        <?php
                            $first = true;
                            foreach($div_option as $opt){
                                echo "<option value=".$opt['id'];
                                if($first){
                                    echo set_select('pil2', $opt['id'], true);
                                    $first = false;
                                } else {
                                    echo set_select('pil2', $opt['id']);
                                }
                                echo ">". $opt['nama'] ."</option>";
                            }
                        ?>
                </select>
            </div>
        </div>
        
        <div class="control-group">
         <label for="pil3" class="control-label">Prioritas Pilihan Divisi 3</label>
            <div class="controls">
                <p class="help-block">Pilih divisi 3.</p>
                <select id="country" name="pil3" class="input-xlarge">
                        <?php
                            $first = true;
                            foreach($div_option as $opt){
                                echo "<option value=".$opt['id'];
                                if($first){
                                    echo set_select('pil3', $opt['id'], true);
                                    $first = false;
                                } else {
                                    echo set_select('pil3', $opt['id']);
                                }
                                echo ">". $opt['nama'] ."</option>";
                            }
                        ?>
                </select>
            </div>
        </div>
        </div>
        <div class="span12">
        <div class="form-actions">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </div>
    </fieldset>
   </form>
   </div>
   </div>