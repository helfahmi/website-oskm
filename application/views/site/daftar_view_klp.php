<div class="container" style="position: relative; top: 50px;">
<div class="row-fluid">
    <legend>Daftar Kelompok OSKM 2013</legend>
        <?php if(validation_errors()) {?>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" >&times;</button>
            <strong>Error!</strong> <?php echo validation_errors(); ?>.
        </div>
      <?php }?>
      <?php echo form_open('daftar_klp/verify',array('class'=>'form-horizontal')); ?>
      <div class="alert alert-info">
        <strong>NIM pertama adalah NIM ketua kelompok.</strong><br/>
        <strong>Untuk mendaftar kelompok seluruh anggota kelompok harus sudah terdaftar.</strong>
      </div>
      <fieldset>
            <!--<div class="">
            <table>
            <div class="control-group">
             <label class="control-label" for="no_klp">No Kelompok:</label>
             <div class="controls">
                 <input type="text" size="20" id="no_klp" value="<?php echo set_value('no_klp'); ?>" class="input-xlarge" name="no_klp"/>
             </div>
            </div> -->
         <?php
            echo "<div class=\"span6\">";
            for($i=1;$i<=ceil($jml_ang/2);$i++){ ?>
                <!-- echo "<tr>\n";
                echo "<td><label for=\"nim$i\">NIM Anggota ke-$i:</label></td>";
                echo "<td><input type=\"text\" size=\"20\" id=\"nim$i\" value=\"";
                echo set_value('nim$i');
                echo "\" name=\"nim$i\"/></td>";
                echo "</tr>"; -->

                <div class="control-group">
                    <label class="control-label" for="nim<?php echo $i;?>">Nim ke <?php echo $i;?></label>
                    <div class="controls">
                        <input type="text" id="nim<?php echo $i;?>" name="nim<?php echo $i;?>" placeholder="NIM anggota <?php echo $i;?>" value="<?php echo set_value('nim'.$i); ?>" class="input-xlarge">
                    </div>
                </div>
            <?php }
            echo "</div>"
            ?>
            <?php
            echo "<div class=\"span6\">";
            for($i=ceil($jml_ang/2)+1;$i<=$jml_ang;$i++){ ?>
                <!-- echo "<tr>\n";
                echo "<td><label for=\"nim$i\">NIM Anggota ke-$i:</label></td>";
                echo "<td><input type=\"text\" size=\"20\" id=\"nim$i\" value=\"";
                echo set_value('nim$i');
                echo "\" name=\"nim$i\"/></td>";
                echo "</tr>"; -->

                <div class="control-group">
                    <label class="control-label" for="nim<?php echo $i;?>">Nim ke <?php echo $i;?></label>
                    <div class="controls">
                        <input type="text" id="nim<?php echo $i;?>" name="nim<?php echo $i;?>" placeholder="NIM anggota <?php echo $i;?>" value="<?php echo set_value('nim'.$i); ?>" class="input-xlarge">
                    </div>
                </div>
            <?php }
            echo "</div>"
            ?>
            <div class="span12">
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </div>
            
            
        </fieldset> 
       </form>
    </div>
</div>
</div>