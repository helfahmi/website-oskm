<div class="row">
	<div class="span4 offset4">
		<?php foreach ($posts as $post): ?>
			<div class="row">
				<b><?php echo $post['title'] ?></b>
				<?php echo $post['content'] ?>
				<i><?php echo $post['author'] ?></i>
				<u><?php echo $post['date'] ?></u>
				<br/><br/><br/><br/><br/>
			</div>
		<?php endforeach ?>
	</div>
</div>