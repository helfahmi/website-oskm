<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>OSKM ITB 2013</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<!--  -->
      <link rel="stylesheet" type="text/css" href="<?php echo lib_url().'bootstrap/css/bootstrap.min.css';?>">
      <link rel="stylesheet" type="text/css" href="<?php echo lib_url().'bootstrap/css/bootstrap-responsive.min.css';?>">
      <script type="text/javascript" src="<?php echo lib_url().'bootstrap/js/bootstrap.min.js';?>"></script>
      <script type="text/javascript" src="<?php echo lib_url().'jquery/jquery.min.js';?>"></script>
    
    

    <?php echo $_scripts; ?>
    <?php echo $_styles; ?>

    <script type="text/javascript">
      $(document).ready(function() {
         $('.carousel').carousel({interval: 60});
      });
    </script>
        <style>
     @font-face{
        font-family : Visit;
        /*src : url('http://oskmitb2013.com/assets/fonts/Visit.ttf');*/
    }
     .visit{
        font-family : Visit;
     }
    </style>
    

    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>
    <?php echo $main_nav; ?>
    <??>
    <?php echo $content ?>
  <div class="container">
    <?php echo $background; ?>
    <!-- <h1>Bootstrap starter template</h1>
    <p>Use this document as a way to quick start any new project.<br> All you get is this message and a barebones HTML document.</p> -->

  </div> 


    

  </body>
</html>