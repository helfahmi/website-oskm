<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title>OSKM ITB 2013</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="author" content="">   
   <script src="<?php echo lib_url().'jquery/jquery.min.js';?>"></script>
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">

   <link href="<?php echo lib_url().'bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">
   <link href="<?php echo lib_url().'bootstrap/css/bootstrap-responsive.min.css'; ?>" rel="stylesheet">
   <script src="<?php echo lib_url().'bootstrap/js/bootstrap.min.js'; ?>"></script>
       <style>
     @font-face{
        font-family : Visit;
        src : url('http://oskmitb2013.com/assets/fonts/Visit.ttf');
    }
     .visit{
        font-family : Visit;
     }
    </style>
   <?= $_scripts ?>
   <?= $_styles ?>   
</head>
<body>
  <?php print $main_nav;?>
  <?php print $background;?>
  <div class="container-fluid">
    <div class="row-fluid">
      <div style="position:relative; top: 50px;">
      <div class="span2">
        <?php print $side_nav;?>
      </div>
      <div class="span10">
        <?php print $content; ?>
      </div>
      </div>
    </div>    
   <?php print $footer; ?>
   </div>   
</body>
</html>
