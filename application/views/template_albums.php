<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>jQuery Wookmark Plug-in Example</title>
  <meta name="description" content="An very basic example of how to use the Wookmark jQuery plug-in.">
  <meta name="author" content="Christoph Ono">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- CSS Reset -->
  <link rel="stylesheet" href="<?php echo lib_url();?>wookmark2/css/reset.css">
  <link href="<?echo lib_url();?>/mason/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  
  <!-- colorbox styles -->
  <link rel="stylesheet" href="<?php echo lib_url();?>wookmark2/example-lightbox/css/colorbox.css">

  <!-- Styling for your grid blocks -->
  <link rel="stylesheet" href="<?php echo lib_url();?>wookmark2/example-lightbox/css/style.css">
  
    
</head>

<body>
  <?php echo $main_nav;?>
  <div id="container" style="position:relative; top:50px;">
    <header>
      Galeri Dokumentasi OSKM
    </header>
    <div id="main" role="main">

      <?php echo $gallery_content;?>

    </div>
    
  </div>

  <!-- include jQuery -->
  <script src="<?php echo lib_url();?>wookmark2/libs/jquery.min.js"></script>

  <!-- Include the imagesLoaded plug-in -->
  <script src="<?php echo lib_url();?>wookmark2/libs/jquery.imagesloaded.js"></script>

  <!-- include colorbox -->
  <script src="<?php echo lib_url();?>wookmark2/libs/jquery.colorbox-min.js"></script>

  <!-- Include the plug-in -->
  <script src="<?php echo lib_url();?>wookmark2/jquery.wookmark.js"></script>

  <!-- Once the page is loaded, initalize the plug-in. -->
  <script type="text/javascript">
    (function ($){
      $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
        var options = {
          autoResize: true, // This will auto-update the layout when the browser window is resized.
          container: $('#main'), // Optional, used for some extra CSS styling
          offset: 2, // Optional, the distance between grid items
          itemWidth: 210 // Optional, the width of a grid item
        };

        // Get a reference to your grid items.
        var handler = $('#tiles li');

        // Call the layout function.
        handler.wookmark(options);

        // Init lightbox
        $('a', handler).colorbox({
          rel: 'lightbox'
        });
      });
    })(jQuery);
  </script>

</body>
</html>
