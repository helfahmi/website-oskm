<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Travels</title>
    <meta name="description" content="Responsive example integrating Twitter Bootstrap and jQuery Masonry">
    <meta name="author" content="Maurizio Conventi">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le styles -->
    <link href="<?echo lib_url();?>/mason/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo lib_url();?>/mason/css/base.css" rel="stylesheet">
    <link href="<?php echo lib_url();?>/mason/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo lib_url();?>/lightbox/css/lightbox.css" rel="stylesheet" />
    <link href="<?php echo lib_url();?>/bootstrap-lightbox/bootstrap-lightbox.min.css" rel="stylesheet" />

</head>

  <body>
        <?php echo $main_nav; ?>
        <div class="container">
            <div id="page-header" class="navbar page-header">
                    <div class="brand">
                        <div>
                            <h1><a href="#" class="home" >My Travels</a></h1>
                            <small>A diary of a life</small>
                        </div>
                    </div>
                <div id="navbar" class="container visible-desktop">
                    <div class="nav-collapse">
                        <ul id="nav" class="nav">
                          <li><a href="#">Home</a></li>
                          <li><a href="#">Agra</a></li>
                          <li><a href="#">San Francisco</a></li>
                          <li><a href="#">London</a></li>
                        </ul>
                    </div>
                </div>

                <select id="collapsed-navbar" class="span12 collapsed-nav hidden-desktop">
                    <option class="page-home" value="#">Home</option>
                    <option class="page-home" value="#">Agra</option>
                    <option class="page-home" value="#">San Francisco</option>
                    <option class="page-home" value="#">London</option>
                </select>
            </div>

            <? echo $gallery_content; ?>

            <!-- <footer>
                  Maurizio Conventi <br />
                  <a href="http://twitter.com/mconventi">Twitter</a>, <a href="https://plus.google.com/u/0/100118193902271276254/">Google+</a>, <a href="http://www.linkedin.com/in/maurizioconventi">LinkedIn</a>
            </footer> -->
        </div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo lib_url();?>mason/js/libs/jquery.js"></script>
    <script src="<?php echo lib_url();?>mason/bootstrap/js/bootstrap.min.js"></script>
    <link href="<?php echo lib_url();?>/lightbox/js/lightbox.js" rel="stylesheet" />
    <link href="<?php echo lib_url();?>/bootstrap-lightbox/bootstrap-lightbox.min.js" rel="stylesheet" />
    <script src="<?php echo lib_url();?>mason/js/libs/jquery.masonry.min.js"></script>
    <script src="<?php echo lib_url();?>mason/js/base.js" type="text/javascript"></script>


</body></html>