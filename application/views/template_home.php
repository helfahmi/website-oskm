<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
  <title>OSKM ITB 2013</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">

    <?php echo $_scripts; ?>
    <?php echo $_styles; ?>
    

    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript">
           $(document).ready(function(){
      // When the document is loaded...
        //$(window).scrollTop();
        // $('.post').height($(window).height()-20);
        // $('.post').css({"min-height ":$(window).height(),"margin-bottom":"20px"});
        $('.post').css({"margin-bottom":$(window).height()-$('.post').height()});
        $('.post').width($(window).width() - 300);

        //$(window).scrollTo($('div#last'), 800 );
      });
      
      function moveToPost(postNum){
        posLeft = $('div.post').get(postNum).offsetLeft - 200;
        posTop = $('div.post').get(postNum).offsetTop - 50;
        $(window).scrollTo({top: posTop, left: posLeft}, 800);
        //$(window).scrollTo($('div.post').get(postNum), 800);
      }
      
      $(document).keypress(function(e){
        // alert($('div.post').get());
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
         $('.carousel').carousel({interval: 4000});
      });
    </script>
    

  </head>
  <body>
    <div class="container">
    <?php echo $background; ?>
    <!-- <h1>Bootstrap starter template</h1>
    <p>Use this document as a way to quick start any new project.<br> All you get is this message and a barebones HTML document.</p> -->

  </div> 
    <?php echo $main_nav; ?>
    <?php echo $side_nav; ?>
    

    <?php echo $content; ?>
  
  </body>
</html>