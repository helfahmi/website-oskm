<!DOCTYPE html>
<html>
<head>
<title>Bootstrap 101 Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<style type="text/css">
.carousel .item {
    width: 100%; /*slider width*/
    max-height: 100%; /*slider height*/
}
.carousel .item img {
    width: 100%; /*img width*/
}
/*add some makeup*/
.carousel .carousel-control {
    background: none;
    border: none;
    top: 50%;
}
/*full width container*/
@media (max-width: 767px) {
    .block {
        margin-left: -20px;
        margin-right: -20px;
    }
}
.item img{
    width : 100%;
    height: auto;
}
</style>
</head>
<body>
    <section class="block">
    <div id="myCarousel" class="carousel slide">
        <div class="carousel-inner">
            <div class="active item">
                <img src="http://lorempixel.com/1024/750" alt="Slide1" />
            </div>
            <div class="item">
                <img src="http://lorempixel.com/1024/750" alt="Slide2" />
            </div>
            <div class="item">
                <img src="http://lorempixel.com/1024/750" alt="Slide3" />
            </div>
        </div>
        <!-- <a class="carousel-control left" href="#myCarousel" data-slide="prev"> &lsaquo; </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next"> &rsaquo; </a> -->

    </div>
</section>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
</body>
</html>


