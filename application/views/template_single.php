<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>Legend - One Page Bootstrap Template</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author"><!-- Styles -->
	<link href="<?php echo lib_url()."legend/";?>css/bootstrap.css" rel="stylesheet">
	<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
	<link href="<?php echo lib_url()."legend/";?>css/style.css" rel="stylesheet">
	<link href="<?php echo lib_url()."legend/";?>css/prettyPhoto.css" id='prettyphoto-css' media='all' rel='stylesheet' type='text/css'>
	<link href="<?php echo lib_url()."legend/";?>css/fontello.css" rel="stylesheet" type="text/css"><!--[if lt IE 7]>
			<link href="css/fontello-ie7.css" type="text/css" rel="stylesheet">  
		<![endif]-->
	<!-- Google Web fonts -->
	<link href='http://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	
	<style>
body {
		padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
	}
	</style>
	<link href="<?php echo lib_url()."legend/";?>css/bootstrap-responsive.css" rel="stylesheet"><!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	<!-- Favicon -->
	<link href="<?php echo lib_url()."legend/";?>img/favicon.ico" rel="shortcut icon"><!-- JQuery -->

	<script src="<?php echo lib_url()."legend/";?>js/jquery.js" type="text/javascript"></script><!-- Load ScrollTo -->

	<script src="<?php echo lib_url()."legend/";?>js/jquery.scrollTo-1.4.2-min.js" type="text/javascript"></script><!-- Load LocalScroll -->

	<script src="<?php echo lib_url()."legend/";?>js/jquery.localscroll-1.2.7-min.js" type="text/javascript"></script><!-- prettyPhoto Initialization -->

	<script charset="utf-8" type="text/javascript">
  $(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto();
		  });
	</script>
</head>

<body>
	<!--******************** NAVBAR ********************-->

	<div class="navbar-wrapper">
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
					<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></a>

					<h1 class="brand"><a href="#top">OSKM</a></h1><!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->

					<nav class="pull-right nav-collapse collapse">
						<ul class="nav" id="menu-main">
							<li>
								<i class="icon-circle"></i>
  			<i class="icon-home"></i>
								<a href="#portfolio" title="portfolio">Berita</a>
							</li>

							<li>
								<a href="#services" title="services">Timeline</a>
							</li>

							<li>
								<a href="#news" title="news">Gallery</a>
							</li>

							<li>
								<a href="#team" title="team">Informasi</a>
							</li>

							<li>
								<a href="#contact" title="contact">Contact</a>
							</li>
						</ul>
					</nav>
				</div><!-- /.container -->
			</div><!-- /.navbar-inner -->
		</div><!-- /.navbar -->
	</div><!-- /.navbar-wrapper -->

	<div id="top"></div><!-- ******************** HeaderWrap ********************-->

	<div id="headerwrap">
		<header class="clearfix">
			<h1>OSKM ITB 2013 <span>#UntukIndonesia!</span> </h1>

			<div class="container">
				<div class="row">
					<div class="span12">
						<h2> As Seen on :</h2>
						<!-- <input class="cform-text" name="your-email" placeholder="you@yourmail.com" size="40" title="your email" type="text"> <input class="cform-submit" type="submit" value="Notify me"> -->
					</div>
				</div>

				<div class="row">
					<div class="span12">
						<ul class="icon">
							<li>
								<a href="http://www.pinterest.com/oskm2013/boards" target="_blank"><em class="icon-pinterest-circled"></em></a>
							</li>

							<li>
								<a href="http://www.facebook.com/oskm2013" target="_blank"><em class="icon-facebook-circled"></em></a>
							</li>

							<li>
								<a href="http://www.twitter.com/oskm2013" target="_blank"><em class="icon-twitter-circled"></em></a>
							</li>

							<!-- <li>
								<a href="#" target="_blank"><em class="icon-gplus-circled"></em></a>
							</li> -->

							<li>
								<a href="#" target="_blank"><em class="icon-skype-circled"></em></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>
	</div><!--******************** Feature ********************-->

	<div class="scrollblock">
		<section id="feature">
			<div class="container">
				<div class="row">
					<div class="span12">
						<article>
							<p>OSKM itu harus gahul.</p>
							<p>- Risky yudha</p>
							<p>, ketua oskm SBM '13</p>
							
							
						</article>
					</div><!-- ./span12 -->
				</div><!-- .row -->
			</div><!-- ./container -->
		</section>
	</div>
	<hr>
	<section>
		<div class="container">
			<div class="timeline_container">
<iframe src='http://embed.verite.co/timeline/?source=0Aqq9bwEtFm49dHEtdnpxR1ZyVXh4cUlJckh0U1M3Qmc&font=DroidSerif-DroidSans&maptype=toner&lang=en&hash_bookmark=true&height=650' width='100%' height='650' frameborder='0'></iframe></div>
		</div>
	</section>
	<!--******************** News Section ********************-->

	<section class="single-page scrollblock" id="news">
		<div class="container">
			<div class="align">
				<em class="icon-pencil-circled"></em>
			</div>

			<h1>Our Blog</h1><!-- Three columns -->

			<div class="row">
				<article class="span4 post">
					<img alt="" class="img-news" src="img/blog_img-01.jpg">

					<div class="inside">
						<p class="post-date"><em class="icon-calendar"></em> March 17, 2013</p>

						<h2>A girl running on a road</h2>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. …</p><a class="more-link" href="#">read more</a>
						</div>
					</div><!-- /.inside -->
				</article><!-- /.span4 -->

				<article class="span4 post">
					<img alt="" class="img-news" src="img/blog_img-02.jpg">

					<div class="inside">
						<p class="post-date">February 28, 2013</p>

						<h2>A bear sleeping on a tree</h2>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. …</p><a class="more-link" href="#">read more</a>
						</div>
					</div><!-- /.inside -->
				</article><!-- /.span4 -->

				<article class="span4 post">
					<img alt="" class="img-news" src="img/blog_img-03.jpg">

					<div class="inside">
						<p class="post-date">February 06, 2013</p>

						<h2>A Panda playing with his baby</h2>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. …</p><a class="more-link" href="#">read more</a>
						</div>
					</div><!-- /.inside -->
				</article><!-- /.span4 -->
			</div><!-- /.row -->
			<a class="btn btn-large" href="#">Go to our blog</a>
		</div><!-- /.container -->
	</section>
	<hr>
	
	<!--******************** Portfolio Section ********************-->

	<section class="single-page scrollblock" id="portfolio">
		<div class="container">
			<div class="align">
				<!-- <em class="icon-desktop-circled"></em> -->
				

			</div>

			<h1 id="folio-headline">Berita</h1>

			<div class="row">
				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-01.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-01.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>Ethan Marcotte Design</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-02.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-02.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>A Book Apart</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-03.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-03.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>Four Rules for Designers</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->
			</div><!-- /.row -->

			<div class="row">
				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-01.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-03.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>Ethan Marcotte Design</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-02.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-01.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>A Book Apart</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="mask2">
						<a href="img/portfolio-03.jpg" rel="prettyPhoto"><img alt="" src="img/portfolio-02.jpg"></a>
					</div>

					<div class="inside">
						<hgroup>
							<h2>Four Rules for Designers</h2>
						</hgroup>

						<div class="entry-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><a class="more-link" href="#">view project</a>
						</div>
					</div><!-- /.inside -->
				</div><!-- /.span4 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	<hr>
	<!--******************** Services Section ********************-->

	<section class="single-page scrollblock" id="services">
		<div class="container">
			<div class="align">
				<em class="icon-cog-circled"></em>
			</div>

			<h1>Services</h1><!-- Four columns -->

			<div class="row">
				<div class="span3">
					<div class="align">
						<em class="icon-desktop sev_icon"></em>
					</div>

					<h2>Web design</h2>

					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div><!-- /.span3 -->

				<div class="span3">
					<div class="align">
						<em class="icon-vector sev_icon"></em>
					</div>

					<h2>Print Design</h2>

					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div><!-- /.span3 -->

				<div class="span3">
					<div class="align">
						<em class="icon-basket sev_icon"></em>
					</div>

					<h2>Ecommerce</h2>

					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div><!-- /.span3 -->

				<div class="span3">
					<div class="align">
						<em class="icon-mobile-1 sev_icon"></em>
					</div>

					<h2>Marketing</h2>

					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div><!-- /.span3 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	<hr>
	<!--******************** Testimonials Section ********************-->

	<section class="single-page hidden-phone" id="testimonials">
		<div class="container">
			<div class="row">
				<div class="blockquote-wrapper">
					<blockquote class="mega">
						<div class="span4">
							<p class="cite">John Doe &amp; Sons:</p>
						</div>

						<div class="span8">
							<p class="alignright">"We highly appreciate the enthusiasm and creative talent of the people at Legend!"</p>
						</div>
					</blockquote>
				</div><!-- /.blockquote-wrapper -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	<hr>
	

	<!--******************** Team Section ********************-->

	<section class="single-page scrollblock" id="team">
		<div class="container">
			<div class="align">
				<em class="icon-group-circled"></em>
			</div>

			<h1>Meet the team</h1><!-- Five columns -->

			<div class="row">
				<div class="span2 offset1">
					<div class="teamalign"><img alt="" class="team-thumb img-circle" src="img/portrait-1.jpg"></div>

					<h3>Andrew</h3>

					<div class="job-position">
						web designer
					</div>
				</div><!-- ./span2 -->

				<div class="span2">
					<div class="teamalign"><img alt="" class="team-thumb img-circle" src="img/portrait-2.jpg"></div>

					<h3>Stephen</h3>

					<div class="job-position">
						web developer
					</div>
				</div><!-- ./span2 -->

				<div class="span2">
					<div class="teamalign"><img alt="" class="team-thumb img-circle" src="img/portrait-3.jpg"></div>

					<h3>Maria</h3>

					<div class="job-position">
						graphic designer
					</div>
				</div><!-- ./span2 -->

				<div class="span2">
					<div class="teamalign"><img alt="" class="team-thumb img-circle" src="img/portrait-4.jpg"></div>

					<h3>John</h3>

					<div class="job-position">
						project manager
					</div>
				</div><!-- ./span2 -->

				<div class="span2">
					<div class="teamalign"><img alt="" class="team-thumb img-circle" src="img/portrait-2.jpg"></div>

					<h3>Ashton</h3>

					<div class="job-position">
						real owner
					</div>
				</div><!-- ./span2 -->
			</div><!-- /.row -->

			<div class="row">
				<div class="span10 offset1">
					<hr class="featurette-divider">

					<div class="featurette">
						<h2 class="featurette-heading">Want to know more? <span class="muted">| a little about us</span></h2>

						<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores.</p>

						<p>At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</p>

						<p>A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.</p>
					</div><!-- /.featurette -->
					<hr class="featurette-divider">
				</div><!-- .span10 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!--******************** Contact Section ********************-->

	<section class="single-page scrollblock" id="contact">
		<div class="container">
			<div class="align">
				<em class="icon-mail-2"></em>
			</div>

			<h1>Contact us now!</h1>

			<div class="row">
				<div class="span12">
					<div class="cform" id="theme-form">
						<form action="#" class="cform-form" method="post">
							<div class="row">
								<div class="span6">
									<span class="your-name"><input class="cform-text" name="your-name" placeholder="Your Name" size="40" title="your name" type="text"></span>
								</div>

								<div class="span6">
									<span class="your-email"><input class="cform-text" name="your-email" placeholder="Your Email" size="40" title="your email" type="text"></span>
								</div>
							</div>

							<div class="row">
								<div class="span6">
									<span class="company"><input class="cform-text" name="company" placeholder="Your Company" size="40" title="company" type="text"></span>
								</div>

								<div class="span6">
									<span class="website"><input class="cform-text" name="website" placeholder="Your Website" size="40" title="website" type="text"></span>
								</div>
							</div>

							<div class="row">
								<div class="span12">
									<span class="message">
									<textarea class="cform-textarea" cols="40" name="message" rows="10" title="drop us a line.">
									</textarea></span>
								</div>
							</div>

							<div>
								<input class="cform-submit pull-left" type="submit" value="Send message">
							</div>

							<div class="cform-response-output"></div>
						</form>
					</div>
				</div><!-- ./span12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	<hr>

	<div class="footer-wrapper">
		<div class="container">
			<footer>
				<small>© 2013 Inbetwin Network. All rights reserved.</small>
			</footer>
		</div><!-- ./container -->
	</div><!-- Loading the javaScript at the end of the page -->
	<script src="js/bootstrap.js" type="text/javascript"></script> <script src="<?php echo lib_url()."legend/";?>js/jquery.prettyPhoto.js" type="text/javascript"></script> <script src="js/site.js" type="text/javascript"></script> <!--ANALYTICS CODE-->	 
</body>
</html>