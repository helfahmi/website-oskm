-- MySQL dump 10.13  Distrib 5.1.37, for Win32 (ia32)
--
-- Host: localhost    Database: oskm2013
-- ------------------------------------------------------
-- Server version	5.1.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `divisi`
--

DROP TABLE IF EXISTS `divisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi`
--

LOCK TABLES `divisi` WRITE;
/*!40000 ALTER TABLE `divisi` DISABLE KEYS */;
INSERT INTO `divisi` VALUES (10,'Divisi Dummy 1'),(11,'Divisi Dummy 2'),(12,'Divisi Dummy 3'),(13,'Divisi Dummy 4');
/*!40000 ALTER TABLE `divisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kelompok`
--

DROP TABLE IF EXISTS `kelompok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelompok` (
  `no_klp` int(12) NOT NULL,
  `nim` int(12) NOT NULL,
  PRIMARY KEY (`nim`),
  UNIQUE KEY `nim` (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kelompok`
--

LOCK TABLES `kelompok` WRITE;
/*!40000 ALTER TABLE `kelompok` DISABLE KEYS */;
/*!40000 ALTER TABLE `kelompok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mahasiswa` (
  `nama` varchar(100) NOT NULL,
  `nim` int(12) NOT NULL,
  `fakultas` varchar(100) NOT NULL,
  `hp` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `penyakit` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `jurusan` varchar(20) NOT NULL DEFAULT 'Belum Dijuruskan',
  PRIMARY KEY (`nim`),
  UNIQUE KEY `nim` (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mahasiswa`
--

LOCK TABLES `mahasiswa` WRITE;
/*!40000 ALTER TABLE `mahasiswa` DISABLE KEYS */;
INSERT INTO `mahasiswa` VALUES ('Fawwaz',13511083,'STEI','+628378432784','Jl Cisitu Lama No 54','Jiwa','adilelfahmi@gmail.com','Pria','Islam','Belum Dijuruskan'),('asdasd',13511047,'asdjkasldjk','+628392498','ajksldjsakld','kjlsadjlkasd','asdd@asd.com','Wanita','Kristen Protestan','Belum Dijuruskan'),('aaa',13511048,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511049,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511050,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511051,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511052,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511053,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511054,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan'),('aaa',13511055,'bbb','7893472','sadsadsa','asdasdsad','asdas@adsasd.com','Pria','Islam','Belum Dijuruskan');
/*!40000 ALTER TABLE `mahasiswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memilih`
--

DROP TABLE IF EXISTS `memilih`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memilih` (
  `nim` int(11) NOT NULL DEFAULT '0',
  `pil1` int(11) NOT NULL DEFAULT '0',
  `pil2` int(11) NOT NULL DEFAULT '0',
  `pil3` int(11) NOT NULL DEFAULT '0',
  `pil4` int(11) NOT NULL DEFAULT '0',
  `pil5` int(11) NOT NULL DEFAULT '0',
  `pil6` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memilih`
--

LOCK TABLES `memilih` WRITE;
/*!40000 ALTER TABLE `memilih` DISABLE KEYS */;
INSERT INTO `memilih` VALUES (13511047,11,12,13,0,0,0),(13511083,10,11,12,0,0,0),(13511046,11,10,13,10,12,13);
/*!40000 ALTER TABLE `memilih` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `author` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (120,'admin','2013-05-28','Selamat Datang','<p>Selamat datang di website <strong>OSKM 2013</strong>! yeahh</p>'),(123,'admin','2013-06-04','asdsadasd','<p>\'sadasdassa\'</p>');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward`
--

DROP TABLE IF EXISTS `reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` date NOT NULL,
  `award1` int(11) NOT NULL,
  `award2` int(11) NOT NULL,
  `award3` int(11) NOT NULL,
  `award4` int(11) NOT NULL,
  `award5` int(11) NOT NULL,
  `award6` int(11) NOT NULL,
  `award7` int(11) NOT NULL,
  `award8` int(11) NOT NULL,
  `award9` int(11) NOT NULL,
  `award10` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward`
--

LOCK TABLES `reward` WRITE;
/*!40000 ALTER TABLE `reward` DISABLE KEYS */;
INSERT INTO `reward` VALUES (1,'2013-06-04',13511047,13511048,13511049,13511050,13511051,13511052,13511053,13511054,13511055,13511083);
/*!40000 ALTER TABLE `reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` VALUES (2,'admin','21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-05  0:20:48
